package com.fitbase.sample;

import java.util.Set;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class chat {
	

	@Test
	public void test() throws InterruptedException {

	//System.setProperty("webdriver.chrome.driver", "./chromedriver.exe");
	
	WebDriver user_1 = new FirefoxDriver();
	WebDriver user_2 = new ChromeDriver();
	
	user_1.navigate().to("https://qa.fitbase.com");
	//user_1.manage().window().maximize();
	
	user_2.navigate().to("https://qa.fitbase.com");
	//user_2.manage().window().maximize();
	
	
	//Log from firefox user1
	Set <String> str = user_1.getWindowHandles();
	System.out.println("Get window handle" + str);
	
	user_1.findElement(By.xpath("//a[text()='SIGN UP / LOG IN']")).click();
	
    Thread.sleep(3000);
    user_1.findElement(By.xpath("//input[@id='username' and @placeholder='Email']")).sendKeys("chatuser1@mailinator.com");
    user_1.findElement(By.xpath("//input[@id='loginpassword']")).sendKeys("password");
    user_1.findElement(By.xpath("//button[@id='login-form']")).click();
	
	Thread.sleep(5000);

	
	//Login from chrom user2
	Set <String> str1 = user_2.getWindowHandles();
	System.out.println("Get window handle" + str1);
	
	user_2.findElement(By.xpath("//a[text()='SIGN UP / LOG IN']")).click();
	
    Thread.sleep(3000);
    user_2.findElement(By.xpath("//input[@id='username' and @placeholder='Email']")).sendKeys("chatuser2@mailinator.com");
    user_2.findElement(By.xpath("//input[@id='loginpassword']")).sendKeys("password");
    user_2.findElement(By.xpath("//button[@id='login-form']")).click();
	
	Thread.sleep(5000);
	
	user_1.findElement(By.xpath("//a[@id='sidebar-parent' and @class='sidebar-toggle']")).click();
	Thread.sleep(3000);
	user_1.findElement(By.xpath("//*[@id=\"addClassc9a42f60-f224-43ce-bbca-2a07c8700ed3\"]")).click();
	
	//delete all conversation
	Thread.sleep(3000);
	user_1.findElement(By.xpath("//a[@data-toggle='dropdown']//i[@class='fa fa-cog']")).click();
	user_1.findElement(By.xpath("//a[text()='Delete all conversation ']")).click();
	Thread.sleep(2000);

	user_1.findElement(By.xpath("//button[text()='Delete Conversation' and @ng-click='deleteAllConversations()']")).click();
	///
	

	user_2.findElement(By.xpath("//*[@id=\"sidebar-parent\"]")).click();
	Thread.sleep(3000);
	user_2.findElement(By.xpath("//a[@class='frnds-box add-cht-mem ng-scope']")).click();
	
	
	//delete all conversation	
	Thread.sleep(3000);
	user_2.findElement(By.xpath("//a[@data-toggle='dropdown']//i[@class='fa fa-cog']")).click();
	user_2.findElement(By.xpath("//a[text()='Delete all conversation ']")).click();
	Thread.sleep(2000);

	user_2.findElement(By.xpath("//button[text()='Delete Conversation' and @ng-click='deleteAllConversations()']")).click();

	
	
	user_1.findElement(By.xpath("//*[@id=\"status_messagefriend.id\"]")).sendKeys("Testing chat functionality... from user1...");
	Thread.sleep(2500);
	
	user_2.findElement(By.xpath("//*[@id=\"status_messagefriend.id\"]")).sendKeys("Working fine chat functionality... from user2...");
	Thread.sleep(2500);
	
	///
	
	user_1.findElement(By.xpath("//*[@id='status_messagefriend.id']")).sendKeys(Keys.RETURN);
	Thread.sleep(5000);

	boolean todaysDateInChatForUser1 = user_2.findElement(By.xpath("//p[text()='Testing chat functionality... from user1...']")).isDisplayed();
	Assert.assertTrue(todaysDateInChatForUser1);
	Thread.sleep(2500);
	
	user_2.findElement(By.xpath("//*[@id='status_messagefriend.id']")).sendKeys(Keys.RETURN);
	Thread.sleep(5000);
	
	boolean todaysDateInChatForUser2 = user_1.findElement(By.xpath("//p[text()='Working fine chat functionality... from user2...']")).isDisplayed();
	Assert.assertTrue(todaysDateInChatForUser2);
	Thread.sleep(2500);

	
	// logging out
	user_1.findElement(By.xpath("//*[@id=\"sidebar-child\"]/div[1]/button")).click();
	Thread.sleep(2500);
	user_1.findElement(By.xpath("//ul[@class='nav navbar-top-links navbar-right userdpw pzero ng-scope']/li/a[@data-toggle='dropdown']")).click();
	Thread.sleep(3000);
	user_1.findElement(By.xpath("//a[contains(text(),'Logout')]")).click();
	
	user_2.findElement(By.xpath("//*[@id=\"sidebar-child\"]/div[1]/button")).click();
	Thread.sleep(2500);
	user_2.findElement(By.xpath("//ul[@class='nav navbar-top-links navbar-right userdpw pzero ng-scope']/li/a[@data-toggle='dropdown']")).click();
	Thread.sleep(3000);
	user_2.findElement(By.xpath("//a[contains(text(),'Logout')]")).click();
	
}
}