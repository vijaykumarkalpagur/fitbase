package com.fitbase.sample;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;


public class SampleTestNGclass {

	@Test
	public void test() throws InterruptedException {

		// To run tests in Chrome browser
		System.setProperty("webdriver.chrome.driver", "./chromedriver.exe");
		WebDriver driver = new ChromeDriver();

		/*
		 * JavascriptExecutor js = (JavascriptExecutor) driver; // This will scroll down
		 * the page by 1000 pixel vertical js.executeScript("window.scrollBy(0,300)");
		 */

		//To run tests in Firefox gecko driver 
		//System.setProperty("webdriver.gecko.driver", "./geckodriver.exe");
	    //WebDriver driver = new FirefoxDriver();
				
		  driver.navigate().to("https://qa.fitbase.com");
		  driver.manage().window().maximize();
		  		  
		  driver.findElement(By.xpath("//a[text()='SIGN UP / LOG IN']")).click();
        //Thread.sleep(1000);
	
		  WebDriverWait wait = new WebDriverWait(driver, 20);
		  wait.until(ExpectedConditions.elementToBeClickable(By.xpath("sdfsd")));
		  
		  driver.findElement(By.xpath("//input[@id='username' and @placeholder='Email']")).sendKeys("data12@mailinator.com");
		  driver.findElement(By.xpath("//input[@id='loginpassword']")).sendKeys("password");
		  driver.findElement(By.xpath("//button[@id='login-form']")).click();
		  
		// Explicit Wait
		/*
		 * WebDriverWait wait = new WebDriverWait(driver, 60);
		 * wait.until(ExpectedConditions.visibilityOfElementLocated(By.
		 * xpath("//a[text()='Live Sessions']")));
		 */
		  
		 // Implicit Wait
		 // driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		  
		  //Thread.sleep(4000);
		 // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		  
		  WebDriverWait wait1 = new WebDriverWait(driver, 40);
		  wait1.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[text()='Live Sessions']")));
		  
		  driver.findElement(By.xpath("//a[text()='Live Sessions']")).click();
		  Thread.sleep(3000);
		 
		//  for(int i=1; i<=5; i++) {
	
						
			List<WebElement> drr = driver.findElements(By.xpath("//div[@class='panel-collapse in']//div[@class='panel-body']/descendant::div"));
			System.out.println(drr.size());
			for(int i3=1; i3<=drr.size(); i3++) {
          //driver.findElement(By.xpath("//*[@id=\"accordion\"]/accordion/div/div[2]/div[1]/h4/a")).click();
          Thread.sleep(3000);
							
				System.out.println(drr.get(i3).getText());
				//if(drr.get(i).getText().contains("Meditation")) {
					drr.get(i3).click();
				//}
					
			}
	//		driver.findElement(By.xpath("//*[@id=\"accordion\"]/accordion/div/div["+i+"]")).click();
			}
	//	  }
		
//		driver.findElement(By.xpath("//label[@for='checkboxBikram Yoga']//parent::div[@class='checkbox']//preceding-sibling::input[@type='checkbox']")).click();
		
		}

		/*
		 * List <WebElement> sss = driver.findElements(By.tagName("input")); //
		 * System.out.println(sss.size());
		 * 
		 * for(int i=1; i<sss.size();i++) { // String str=sss.get(i).getText(); //
		 * System.out.println(str); }
		 * 
		 * Thread.sleep(1000);
		 * 
		 * //driver.findElement(By.
		 * xpath("//div[@class='col-sm-12']//ul[li[9]]//a[text()='Live Sessions']")).
		 * click(); //driver.findElement(By.xpath(
		 * "//*[@ng-if=\"pageTitle!=='Live'\"]//div//div//div//div//ul//li[9]//a")).
		 * click(); driver.findElement(By.xpath(
		 * "//*[@id='schedulewkt-list-wrapper']/div[1]/a/div[2]/div[2]/h4")).click();
		 * //*[@id='schedulewkt-list-wrapper']/div[1]/a/div[2]/div[2]/h4
		 * Thread.sleep(3000);
		 * 
		 * JavascriptExecutor js = (JavascriptExecutor) driver; // This will scroll down
		 * the page by 1000 pixel vertical js.executeScript("window.scrollBy(0,500)");
		 * 
		 * 
		 * driver.findElement(By.xpath("//button[text()='Enroll Now']")).click();
		 * Thread.sleep(1000);
		 * 
		 * driver.findElement(By.
		 * xpath("//button[@type='button' and @ng-click='payNow(lookup)']")).click();
		 * Thread.sleep(3000);
		 * driver.findElement(By.xpath("//*[@id=\"paymentDisable\"]/ul/li[2]")).click();
		 * 
		 * // driver.findElement(By.
		 * xpath("//div[@id='paymentDisable']//ul//li[2]//a[text()='Credit/Debit Card']"
		 * )).click(); Thread.sleep(4000);
		 * 
		 * driver.findElement(By.xpath("//*[@id=\"card-element\"]/div/iframe")).sendKeys
		 * ("5105105105105100"); Thread.sleep(2000);
		 * 
		 * driver.findElement(By.xpath("//*[@id=\"paymentDisable\"]/ul/li[3]")).click();
		 * Thread.sleep(2000);
		 * 
		 * driver.findElement(By.xpath("//*[@id=\"paymentDisable\"]/ul/li[2]")).click();
		 * Thread.sleep(2000);
		 * 
		 * driver.findElement(By.xpath("//*[@id=\"paymentDisable\"]/ul/li[1]")).click();
		 * 
		 * 
		 * String str = driver.getTitle().toUpperCase();
		 * System.out.println("Title of the web page :"+str);
		 * 
		 * int str2 = driver.getTitle().length();
		 * System.out.println("Length of the web page :"+str2);
		 * 
		 * String str1 = driver.getCurrentUrl().toUpperCase();
		 * System.out.println("Current URL of the page :"+str1);
		 * 
		 * driver.navigate().to("http://www.fitbase.com/app/login");
		 * 
		 * driver.navigate().back(); driver.navigate().forward();
		 * driver.navigate().back();
		 * 
		 * //WebElement parentElement = driver.findElement(By.className("login"));
		 * //WebElement childElement =
		 * //parentElement.findElement(By.linkText("SIGN UP / LOG IN"));
		 * Thread.sleep(2000);
		 * 
		 * WebElement ele =
		 * driver.findElement(By.xpath("//a[@data-reveal-id='login']"));
		 * 
		 * WebElement parent = driver.findElement(By.className("div")); WebElement
		 * child=parent.findElement(By.id("sign-in-google")); child.click();
		 * 
		 * String strs = ele.getTagName(); System.out.println("tag name is "+ strs);
		 * ele.click();
		 * 
		 * WebElement elee= driver.findElement(By.id("dgdf"));
		 * org.openqa.selenium.Dimension dimm=elee.getSize();
		 * System.out.println(dimm.height+dimm.width);
		 * 
		 * By c1ele=By.id("gfhg"); WebElement f777d = driver.findElement(c1ele);
		 * 
		 * WebElement ele123 =
		 * driver.findElement(By.xpath("//*[@id=\"login\"]/div/div/div/button"));
		 * org.openqa.selenium.Dimension dim = ele123.getSize();
		 * System.out.println("Window size is "+dim); System.out.println("Height is " +
		 * dim.height +" Width is " + dim.width);
		 * 
		 * WebElement u7777= driver.findElement(By.id("")); u7777.click();
		 * 
		 * String strr = "PRIYA"; int i = strr.length(); StringBuffer strb = new
		 * StringBuffer(); for(int j=i-1; j>=0; j--) { strb =
		 * strb.append(strr.charAt(j)); } System.out.println(strb);
		 * 
		 * }
		 * 
		 */
	
