package com.fitbase.qa.helper;

import java.io.FileInputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import com.fitbase.qa.utilities.Log;

public class Browserhelper {
	public static WebDriver driver;
	 
public static WebDriver openBrowser() throws Exception{

	Properties prop = new Properties();
	FileInputStream input  = new FileInputStream("C:\\Users\\Testing\\Documents\\Fitbase_Web_Checklist\\src\\test\\java\\com\\fitbase\\qa\\config\\config.properties");
	prop.load(input);
	String browserName = prop.getProperty("browser");
	
	try {
		if(browserName.equalsIgnoreCase("Chrome")){
			System.setProperty("webdriver.chrome.driver", "C:\\Users\\Testing\\Documents\\Fitbase_Web_Checklist\\chromedriver.exe");	
				driver = new ChromeDriver();
				Log.info("Chrome Driver initiated ");
				}
		
		else if(browserName.equalsIgnoreCase("FireFox")){
		    System.setProperty("webdriver.gecko.driver", ".\\geckodriver.exe");
			    driver = new FirefoxDriver();
			  //  Log.info("FireFox Driver initiated ");
			    }
		
		else if(browserName.equalsIgnoreCase("ie")){
			System.setProperty("webdriver.ie.driver",".\\IEDriverServer.exe");
				driver = new InternetExplorerDriver();
			//	Log.info("InternetExplorer1 Driver initiated ");
				}
	}		
				catch (Exception e) {
					e.getMessage();
					}
	driver.get(prop.getProperty("url"));
	//Log.info("Fitbase home page opened");
	driver.manage().timeouts().pageLoadTimeout(3000, TimeUnit.SECONDS);
	driver.manage().window().maximize();
	driver.manage().deleteAllCookies();
	driver.manage().timeouts().pageLoadTimeout(4000, TimeUnit.SECONDS);	
	return driver;
    }
}
	
