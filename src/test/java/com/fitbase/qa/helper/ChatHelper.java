package com.fitbase.qa.helper;

import java.util.Set;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;

import com.fitbase.qa.utilities.Log;

public class ChatHelper {
	
	public static void chatFunctionality() throws InterruptedException {
	
//	WebDriver user_1 = new FirefoxDriver();
	WebDriver user_1 = new ChromeDriver();
	WebDriver user_2 = new ChromeDriver();

	
	user_1.navigate().to("https://qa.fitbase.com");
	Log.info("Opened acceptance fitbase url in Firefox browser....");
	//user_1.manage().window().maximize();
	
	user_2.navigate().to("https://qa.fitbase.com");
	Log.info("Opened acceptance fitbase url in Chrome browser....");
	//user_2.manage().window().maximize();
	
	
	//Log from Firefox user1
	Set <String> str = user_1.getWindowHandles();
	System.out.println("Get window handle" + str);
	
	user_1.findElement(By.xpath("//a[text()='SIGN UP / LOG IN']")).click();
	Log.info("Click action performed on signup/login link in Firefox browser....");
	
    Thread.sleep(3000);
    user_1.findElement(By.xpath("//input[@id='username' and @placeholder='Email']")).sendKeys("chatuser1@mailinator.com");
    user_1.findElement(By.xpath("//input[@id='loginpassword']")).sendKeys("password");
    user_1.findElement(By.xpath("//button[@id='login-form']")).click();
	Log.info("Logged-in with normal user in Firefox browser....");
	Thread.sleep(5000);
	
	//Login from chrom user2
	Set <String> str1 = user_2.getWindowHandles();
	System.out.println("Get window handle" + str1);
	
	user_2.findElement(By.xpath("//a[text()='SIGN UP / LOG IN']")).click();
	Log.info("Click action performed on signup/login link in Chrome browser....");

    Thread.sleep(3000);
    user_2.findElement(By.xpath("//input[@id='username' and @placeholder='Email']")).sendKeys("chatuser2@mailinator.com");
    user_2.findElement(By.xpath("//input[@id='loginpassword']")).sendKeys("password");
    user_2.findElement(By.xpath("//button[@id='login-form']")).click();
	Log.info("Logged-in with normal user in Chrome browser....");
	Thread.sleep(5000);
	
	user_1.findElement(By.xpath("//a[@id='sidebar-parent' and @class='sidebar-toggle']")).click();
	Log.info("Click action performed on slider in Firefox browser....");
	Thread.sleep(3000);
	user_1.findElement(By.xpath("//*[@id=\"addClassc9a42f60-f224-43ce-bbca-2a07c8700ed3\"]")).click();
	Log.info("Click action performed on chat user in slider in Firefox browser....");
	
	//delete all conversation from user1
	Thread.sleep(3000);
	user_1.findElement(By.xpath("//a[@data-toggle='dropdown']//i[@class='fa fa-cog']")).click();
	user_1.findElement(By.xpath("//a[text()='Delete all conversation ']")).click();
	Thread.sleep(2000);

	user_1.findElement(By.xpath("//button[text()='Delete Conversation' and @ng-click='deleteAllConversations()']")).click();
	Log.info("First deleted all conversations from chat in Firefox browser....");

	//Click on chat search box
	user_2.findElement(By.xpath("//*[@id=\"sidebar-parent\"]")).click();
	Thread.sleep(3000);
	user_2.findElement(By.xpath("//a[@class='frnds-box add-cht-mem ng-scope']")).click();
	Log.info("Click action performed on chat user in slider in Chrome browser....");

	//delete all conversation	
	Thread.sleep(3000);
	user_2.findElement(By.xpath("//a[@data-toggle='dropdown']//i[@class='fa fa-cog']")).click();
	user_2.findElement(By.xpath("//a[text()='Delete all conversation ']")).click();
	Thread.sleep(2000);

	user_2.findElement(By.xpath("//button[text()='Delete Conversation' and @ng-click='deleteAllConversations()']")).click();
	Log.info("First deleted all conversations from chat in Chrome browser....");

	// Type text from user1
	user_1.findElement(By.xpath("//*[@id=\"status_messagefriend.id\"]")).sendKeys("Testing chat functionality... from user1...");
	Thread.sleep(2500);
	Log.info("Entered text in user1 chat textbox...");

	// Type text from user2
	user_2.findElement(By.xpath("//*[@id=\"status_messagefriend.id\"]")).sendKeys("Working fine chat functionality... from user2...");
	Thread.sleep(2500);
	Log.info("Entered text in user1 chat textbox...");

	
	// Send text from user1
	user_1.findElement(By.xpath("//*[@id='status_messagefriend.id']")).sendKeys(Keys.RETURN);
	Thread.sleep(5000);
	Log.info("Send text from user1 to user2....");

	// Verify chat functionality text from user2
	boolean todaysDateInChatForUser1 = user_2.findElement(By.xpath("//p[text()='Testing chat functionality... from user1...']")).isDisplayed();
	Assert.assertTrue(todaysDateInChatForUser1);
	Log.info("Verified user text in user1 textbox...");
	Thread.sleep(2500);
	
	// Send text from user2
	user_2.findElement(By.xpath("//*[@id='status_messagefriend.id']")).sendKeys(Keys.RETURN);
	Log.info("Send text from user2 to user1....");
	Thread.sleep(5000);
	
	// Verify chat functionality text from user1
	boolean todaysDateInChatForUser2 = user_1.findElement(By.xpath("//p[text()='Working fine chat functionality... from user2...']")).isDisplayed();
	Assert.assertTrue(todaysDateInChatForUser2);
	Log.info("Verified user text in user2 textbox...");
	Thread.sleep(2500);

	
	// logging out
	user_1.findElement(By.xpath("//*[@id=\"sidebar-child\"]/div[1]/button")).click();
	Log.info("Click action performed on slider close icon... in firefox browser...");
	
	Thread.sleep(2500);
	user_1.findElement(By.xpath("//ul[@class='nav navbar-top-links navbar-right userdpw pzero ng-scope']/li/a[@data-toggle='dropdown']")).click();
	Thread.sleep(3000);
	user_1.findElement(By.xpath("//a[contains(text(),'Logout')]")).click();
	Log.info("Logged out successfully from user1... in firefox browser...");

	
	user_2.findElement(By.xpath("//*[@id=\"sidebar-child\"]/div[1]/button")).click();
	Log.info("Click action performed on slider close icon... in chrome browser...");

	Thread.sleep(2500);
		
    user_2.findElement(By.xpath("//ul[@class='nav navbar-top-links navbar-right userdpw pzero ng-scope']/li/a[@data-toggle='dropdown']")).click();
	Thread.sleep(3000);
	user_2.findElement(By.xpath("//a[contains(text(),'Logout')]")).click();
	Log.info("Logged out successfully from user2... in chrome browser...");

	
	user_1.quit();
	Log.info("Firefox driver quit...");

	user_2.quit();
	Log.info("Chrome driver quit...");

	
}
}
