package com.fitbase.qa.actions;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import com.fitbase.qa.pages.LiveConnectPage;
import com.fitbase.qa.pages.LoginPage1;
import com.fitbase.qa.utilities.Log;

public class LiveConnectVideoAction {

	public static WebDriver driver;

	public static void checkLiveConnectVideo(WebDriver driver) throws Exception {
		LiveConnectPage liveConnect = new LiveConnectPage(driver);
		LoginPage1 login = new LoginPage1(driver);
		WebDriverWait wait = new WebDriverWait(driver, 80);

		wait.until(ExpectedConditions.elementToBeClickable(liveConnect.btn_LiveSessionsNotificationsPopup)).click();
		Log.info("Click action performed on live sessions pop-up to check live sessions...");

		
			if (liveConnect.msg_NoLiveSessionsAvailable.isDisplayed() == false) {
				wait.until(ExpectedConditions.elementToBeClickable(liveConnect.btn_LiveSession)).click();
				Log.info("Click action performed on active live button to check video...");
				Thread.sleep(18000);
				
			
			  Set<String> window = driver.getWindowHandles(); 
			  Iterator<String> it = window.iterator();
			  
			  String parentWindow = it.next(); 
			  String childWindow = it.next();
			 // String childWindow1 = it.next();
			  driver.switchTo().window(childWindow);
			  Log.info("Switching to live video sessions page..."); 
			  
			        // Switching to Alert        
		        Alert alert = driver.switchTo().alert();		
		        		
		        // Capturing alert message.    
		        String alertMessage= driver.switchTo().alert().getText();		
		        		
		        // Displaying alert message		
		        System.out.println(alertMessage);	
		        Thread.sleep(5000);
		        		
		        // Accepting alert		
		        alert.accept();	
			  		
				//Assert.assertEquals(driver.getTitle(), "Live");
				Log.info("Live video page iv s verified successfully...");
				
		//		Thread.sleep(2000);
			
			
				driver.switchTo().activeElement().click();
			

				
				Log.info("Switching back to parent widnow to logout...");
				
	//			LiveConnectVideoAction.TrainerLogin(driver);
				 

			} else {
				Log.info("There are no live sessions available to check...");
			}
			
	} /*
		 * catch (Exception e) { } finally {
		 * driver.manage().timeouts().implicitlyWait(1000, TimeUnit.SECONDS); } }
		 */
public static void TrainerLogin(WebDriver driver) throws Exception{
		
		LoginPage1 login = new LoginPage1(driver);
		
		login.TrainerUsername.isDisplayed();
		login.TrainerUsername.clear();
		login.TrainerUsername.sendKeys("trainer@mailinator.com");	
		Thread.sleep(2000);
		Log.info("Entered Trainer valid username");
		
		login.TrainerPassword.isDisplayed();
		login.TrainerPassword.clear();
		login.TrainerPassword.sendKeys("password");
		Thread.sleep(2000);
		Log.info("Entered Trainer valid Password");
		
		login.TrainerLoginBtn.isDisplayed();
		login.TrainerLoginBtn.isEnabled();	
		login.TrainerLoginBtn.click();	
		Thread.sleep(5000);
		
		Assert.assertEquals("Overview" ,"Overview" );
		Log.info("Click action performed on Loginbutton and succesfully Login into Application");
		
		Thread.sleep(5000);
	}
}
