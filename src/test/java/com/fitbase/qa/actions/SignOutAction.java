package com.fitbase.qa.actions;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.fitbase.qa.pages.LogoutPage;
import com.fitbase.qa.utilities.Log;

public class SignOutAction {
	static WebDriver driver;
	
	public static void signout(WebDriver driver) throws InterruptedException {
		
		LogoutPage logout=new LogoutPage(driver);
		
		Thread.sleep(2000);
		WebDriverWait wait = new WebDriverWait(driver, 60);
		Thread.sleep(2000);
		Log.info("Implicit wait applied on driver to load the page...");
		
		wait.until(ExpectedConditions.elementToBeClickable(logout.drp_Menu)).click();	
		Log.info("Click action perfomed on dropdown menu...");
		
		Thread.sleep(3000);
		wait.until(ExpectedConditions.elementToBeClickable(logout.lnk_Logout)).click();
		Log.info("Click action performed on logout link...");
		Thread.sleep(2000);

		// Waiting to get page title...
		wait.until(ExpectedConditions.titleIs("Fitbase - Wellness. Anywhere. Anytime."));
		
		Assert.assertTrue(driver.getTitle().equalsIgnoreCase("Fitbase - Wellness. Anywhere. Anytime."), "Fitbase - Wellness. Anywhere. Anytime.");
		Log.info("Logged out successfully and verified Fitbase static site page title after logging out...");

	}
}
