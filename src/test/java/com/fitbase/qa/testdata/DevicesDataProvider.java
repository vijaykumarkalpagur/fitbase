package com.fitbase.qa.testdata;

import org.testng.annotations.DataProvider;

public class DevicesDataProvider {

	@DataProvider(name="FitbitDevice")
	public static Object[][] getDataFromFitbitDevice(){
		return new Object[][] {
			{"priyaindugula22@gmail.com"}, 
			{"layalaya"}};	
	}
	
	@DataProvider(name="MisfitDevice")
	public static Object[][] getDataFromMisfitDevice(){
		return new Object[][] {{"priyaindugula22@gmail.com"}, {"password"}};
	}
	
	@DataProvider(name="WithingsDevice")
	public static Object[][] getDataFromWithingsDevice(){
		return new Object[][] {
			{"priyaindugula22@gmail.com"},
			{"password"}
		};
	}
	
	@DataProvider(name="Strava")
	public static Object[][] getDataProviderStravaDevice(){
		return new Object[][] {{"priyaindugula22@gmail.com", "password"}};
	}
}
