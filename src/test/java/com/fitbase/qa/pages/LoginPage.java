package com.fitbase.qa.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class LoginPage{
    
	static WebDriver driver;

    public LoginPage(WebDriver driver) {
	PageFactory.initElements(driver, this);
	}
	
    //##### Normal login user ##########
    ////////////////////////////////////
	@FindBy(how = How.XPATH, using ="//*[@id='signuplogin']/a")
	@CacheLookup
	public WebElement btn_LoginAndSignUp;
	
	@FindBy(xpath="//*[@id=\'username\']")
	public WebElement txt_UserName;
	
	@FindBy(xpath="//*[@id='loginpassword']")
	public WebElement txt_Password;
		
	@FindBy(xpath="//button[@id='login-form']")
	public WebElement btn_Submit;
	
	
	//######### Google login user ##########
	////////////////////////////////////////
	@FindBy(xpath="//div[@id='sign-in-google']")
	public WebElement tab_GoogleLogin;
	
	@FindBy(xpath="//input[@type='email']")
	public WebElement txt_Gmail_Email;
	
	@FindBy(xpath="//div[@id='identifierNext']")
	public WebElement btn_GmailEmail_Next;
	
	@FindBy(xpath="//input[@type='password']")
	public WebElement txt_Gmail_Password;
	
	@FindBy(xpath="//*[@id=\'passwordNext\']")
	public WebElement btn_GmailPassword_Next;
	
	@FindBy(xpath="//*[@id='yDmH0d']")
	public WebElement GmailAllowButton;
	
	
	
	//########### Facebook user ##############
	/////////////////////////////////////////
	@FindBy(how=How.XPATH, using="//div[@id='sign-in-fb']")
	public WebElement tab_FacebookLogin;
	
	@FindBy(how=How.XPATH, using="//input[@id='email']")
	public WebElement txt_Facebook_UserName;
	
	@FindBy(how=How.XPATH, using="//input[@type='password']")
	public WebElement txt_Facebook_Password;
	
	@FindBy(how=How.XPATH, using="//label[@id='loginbutton']")
	public WebElement btn_FacebookLogin;
	
	
	//########### Forgot password ###########
	/////////////////////////////////////////
	@FindBy(how=How.XPATH, using="//a[@id=\"forgotpassword\"]")
	public WebElement lnk_fotgotPassword;
	
	@FindBy(how=How.XPATH, using="//*[@id=\"emailid\"]")
	public WebElement txt_forgotPassword;
	
	@FindBy(how=How.XPATH, using="//*[@id='resetpassword']")
	public WebElement btn_forgotPassword;

	@FindBy(how=How.XPATH, using="//*[@id='resetpassword-success']")
	public WebElement msg_forgotPasswordSuccessMessage;

    }
