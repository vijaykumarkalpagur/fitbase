package com.fitbase.qa.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class ChatPage {
	public static WebDriver driver;
	public ChatPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	// Slider & chat user page objects
	@FindBy(how=How.XPATH, using="//a[@id='sidebar-parent' and @class='sidebar-toggle']")
	public WebElement slider_chat;
	
	@FindBy(xpath="//*[@id='addClassc9a42f60-f224-43ce-bbca-2a07c8700ed3']")
	public WebElement chat_User;
	
	//Delete all conversation page objects
	@FindBy(how=How.XPATH, using="//a[@data-toggle='dropdown']//i[@class='fa fa-cog']")
	public WebElement dropdown_Delete;
	
	@FindBy(how=How.XPATH, using="//a[text()='Delete all conversation']")
	public WebElement lnk_DeleteAll;
	
	@FindBy(how=How.XPATH, using="//button[text()='Delete Conversation' and @ng-click='deleteAllConversations()']")
	public WebElement btn_ConfirmDeleteAll;
	
	// Chat textbox
	@FindBy(xpath="//*[@id='status_messagefriend.id']")
	public WebElement txt_ChatBox;


	@FindBy(xpath="//p[text()='Testing chat functionality... from user1...']")
	public WebElement txt_receiveTextFromUser1;

	@FindBy(xpath="//p[text()='Working fine chat functionality... from user2...']")
	public WebElement txt_receiveTextFromUser2;


}
