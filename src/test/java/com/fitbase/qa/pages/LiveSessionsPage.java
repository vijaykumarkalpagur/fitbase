package com.fitbase.qa.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class LiveSessionsPage {

	static WebDriver driver;

	public LiveSessionsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);		
	}
	
	//########### Live session page objects #############
	@FindBy(how=How.XPATH, using="//a[text()='Live Sessions']")
	@CacheLookup
	public WebElement lnk_LiveSessions;
	
	@FindBy(xpath="/html/body/jhi-main/jhi-livesession/div/div[2]/div/div[2]/div[2]/div[1]/div/div[1]/button")
	public static WebElement Clickonfitlericon;
	
	@FindBy(xpath="//*[@id=\'filter\']/div/div/div[3]/select")
	public static WebElement ClickonsessionPrice;
	
	@FindBy(xpath="//*[@id=\'filter\']/div/div/div[3]/select/option[3]")
	public WebElement drp_SortByPrice;
	
	@FindBy(how=How.XPATH, using="/html/body/jhi-main/jhi-livesession/div/div[2]/div/div[2]/div[2]/div[2]/div/div/a/div/div[2]")
	public WebElement lnk_WorkoutSession;
	
	@FindBy(how=How.XPATH, using="/html/body/jhi-main/jhi-livesessiondetails/div/div[2]/div/div[1]/div[2]/div/div/div[3]/div/div[2]/div/button")
	public WebElement btn_EnrollNowInSessionPreviewPage;
	
	@FindBy(xpath="//*[@id=\'toast-container\']/div/div")
	public WebElement btn_ConflictConfirmation;
	
	@FindBy(how=How.XPATH, using="/html/body/ngb-modal-window/div/div/div/div[3]/button")
	public WebElement btn_EnrollNowInConfirmationPopUp;
	
	@FindBy(how=How.XPATH, using="//div[@id='paymentDisable']//ul//li[1]//a[text()='Saved Cards']")
	public WebElement tab_SavedCards;
	
	@FindBy(how=How.XPATH, using="//div[@id='paymentDisable']//ul//li[2]//a[text()='Credit/Debit Card']")
	public WebElement tab_CredirOrDebitCard;
	
	
	@FindBy(how=How.XPATH, using="//*[@id='card-element']/div/iframe")
	public WebElement txt_CardNumber;
		
	@FindBy(how=How.XPATH, using="//*[@id=\'paymentDisable\']/ul/li[3]")
	public WebElement tab_PayPal;
	
	@FindBy(xpath="//button[@class='pay ng-binding']")
	public WebElement btn_PayInCheckoutPage;

	@FindBy(xpath="//*[@id=\'paypal\']/div/button")
	public WebElement btn_PayButtonUnderPayPalTab;
	
	@FindBy(xpath="//input[@id='email']")
	public WebElement txt_PayPal_EmailAddress;
	
	@FindBy(how=How.XPATH, using="//input[@placeholder='CVC']")
	public WebElement txt_CVC;

	@FindBy(how=How.XPATH, using="//input[@aria-label='Credit or debit card expiration date' and @placeholder='MM / YY']")
	public WebElement txt_MonthAndYear;
	
	@FindBy(xpath="//button[@id='btnNext']")
	public WebElement btn_Next_PayPalEmailAddress;
	
	@FindBy(xpath="//input[@id='password']")
	public WebElement txt_PayPal_Password;
	
	@FindBy(xpath="//button[@id='btnLogin']")
	public WebElement btn_PayPal_Login;
	
	@FindBy(xpath="//*[@id=\'payment-submit-btn\']")
	public WebElement btn_PayPalPayment_Continue;
	
	@FindBy(how=How.XPATH, using="/html/body/jhi-main/jhi-livesession/div/div[2]/div/div[2]/div[2]/div[4]/div/div/div/span")
	public WebElement msg_HelpMessageWhenThereAreNoWorkoutsAvailable;

	@FindBy(xpath="//span[@class='label label-success']")
	public WebElement paymentSuccessPage;
}







