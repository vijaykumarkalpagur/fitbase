package com.fitbase.qa.pages;


	import org.openqa.selenium.WebDriver;
	import org.openqa.selenium.WebElement;
	import org.openqa.selenium.support.FindBy;
	import org.openqa.selenium.support.How;
	import org.openqa.selenium.support.PageFactory;

	public class LoginPage1 {
		static WebDriver driver;
		
	public LoginPage1(WebDriver driver){
		
		PageFactory.initElements(driver, this);
	}
		@FindBy(xpath = "//*[@id='username']")
		public WebElement TrainerUsername;

		@FindBy(how=How.XPATH,using= "//*[@id='loginpassword']")
		//@FindBy(how=How.XPATH, using="//*[@id='password']")
		public WebElement TrainerPassword;

		@FindBy(how=How.XPATH,using = "//button[@id='login-form' and @type='submit']")
		public WebElement TrainerLoginBtn;

	}

