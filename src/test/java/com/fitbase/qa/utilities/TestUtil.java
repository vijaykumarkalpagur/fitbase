package com.fitbase.qa.utilities;

import java.math.BigInteger;
import java.security.SecureRandom;

public class TestUtil {

	public static long PAGE_LOAD_TIMEOUT = 20;
	public static long IMPLICIT_WAIT =10;
	
	public static String generateRandomString() { return "test" + new BigInteger(10, new SecureRandom()).intValue(); }
}
