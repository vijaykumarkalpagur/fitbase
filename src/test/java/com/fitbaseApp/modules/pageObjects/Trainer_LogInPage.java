package com.fitbaseApp.modules.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class Trainer_LogInPage {

		public Trainer_LogInPage(WebDriver driver){
			PageFactory.initElements(driver, this);
			}

		// Static page button
		@FindBy(xpath=".//*[@id='hidemenu1']/li[7]/a")              
		public WebElement btnTrainerSignUpInStaticPage;
		
		//Trainer page
		//@FindBy(xpath=".//*[@id='signuplogin']/a")
		@FindBy(xpath="//li[@class='login']/a")
	    public WebElement mainSignUpLoginBtn;
		
		// Facebook button
		@FindBy(xpath=".//*[@id='sign-in-fb']")
		public WebElement facebookBtn;
		
		// Gmail button
		@FindBy(xpath=".//*[@id='sign-in-google']")
		public WebElement googleBtn;
		
		// FACEBOOK
		@FindBy(xpath = ".//*[@id='sign-in-fb']")
	   	public WebElement btn_facebook;
	   	
	    // @FindBy(xpath = ".//*[@id='email']")
	    @FindBy(id = "email")
	   	public WebElement txt_facebookUserName;
	   	
	    //	@FindBy(xpath = ".//*[@id='pass']")
	    @FindBy(id = "pass")
	   	public WebElement txt_facebookPassword;
	   	
	   	@FindBy(name = "login")
	   	public WebElement btn_facebookLogin;
	   	
	   	//GMAIL
	   	
	   	@FindBy(xpath = ".//*[@id='sign-in-google']")
	   	public WebElement btn_gmail;
	   	
	   	//@FindBy(xpath = ".//*[@id='Email']")
	   	@FindBy(name="identifier")
	   	public WebElement txt_gmailUserName;
	   	
	   	@FindBy(xpath = "//Content[@class='CwaK9']//span[contains(text(),'Next')]")
	   	public WebElement btn_gmailNext;
	   	
	   	@FindBy(xpath = "//input[@class='whsOnd zHQkBf']")
	   	public WebElement txt_gmailPassword;
	   	
	   	@FindBy(xpath = "//span[@class='RveJvd snByac']")
	   	public WebElement btn_gmailLogin;
	   	
	   	
	   	//********** Normal login page objects*****************//
		@FindBy(xpath = ".//*[@id='username']")
		public WebElement txt_UserName;
		
		@FindBy(xpath = ".//*[@id='loginpassword']")
		public WebElement txt_Password;
		
		@FindBy(css = "#user-login-form > #login-form")
		public WebElement btn_Login;
		
		//**************Log out****************//
		@FindBy(xpath="/html/body/div/nav/div[2]/div/div/div/div[1]/ul[1]/li/a")
		public WebElement drp_Menu;
		
		@FindBy(xpath="/html/body/div/nav/div[2]/div/div/div/div[1]/ul[1]/li/ul/li[8]/a")
		public WebElement lnk_Logout;
				
		
}


   	

