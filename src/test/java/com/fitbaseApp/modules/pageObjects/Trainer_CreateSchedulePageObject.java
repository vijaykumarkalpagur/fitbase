package com.fitbaseApp.modules.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Trainer_CreateSchedulePageObject {

	public Trainer_CreateSchedulePageObject(WebDriver driver){
		PageFactory.initElements(driver, this);
		}

	// Create schedule link
	//@FindBy(xpath=".//*[@id='navbar']/ul/li[3]/a")  
	@FindBy(xpath="//a[@ng-href='/trainer/#/schedule/' and @href='/trainer/#/schedule/']")
	public WebElement lnk_CreateSchedule;
	
	// Name of the Workout Session
	@FindBy(id="name")
	public WebElement txt_NameOfTheWorkoutSession;
	
	// Duration
	@FindBy(xpath="/html/body/div/section/div/div/div[1]/div/div/div/div/div/div/form/div[3]/div/div[2]/div[2]/div/div[2]/div[5]/div/div[1]/fieldset/div[3]/div/div/select")
	public WebElement txt_Duration;
	
	
	// Type of workout
	@FindBy(xpath="html/body/div[1]/section/div/div/div[1]/div/div/div/div/div/div/form/div[1]/div[2]/div/select")
	public WebElement dropdown_TypeOfWorkoutSession;
	
	// Select group workout session dropdown value
	@FindBy(xpath="//div[@class='form-group']//option[@value='1-ON-1 Workout']/../../select")
	public WebElement dropdown_SelectOfWorkoutSession;
		
	// Training level
	@FindBy(xpath="//form[@name='createScheduleForm']//label[@for='training']/../select")
	public WebElement dropdown_TrainingLevel;
	
	// Select Expert level
	@FindBy(xpath="//form[@name='createScheduleForm']//label[@for='training']/../select/option[text()='Expert']")
	public WebElement dropdown_SelectExpertLevel;
	
    /////////////////////////////////////////////////////////////////////////////////////////////////
    //Single session
    /////////////////////////////////////////////////////////////////////////////////////////////////
	
	
	// Single session on date field
	@FindBy(id="dateid")
	public WebElement txt_singleSessionOnDateField;
	
	// Single session starts from time field
	@FindBy(name="singleschedulestartTime")
	public WebElement txt_singleSessionStartsFromDateField;
	
	
	
	/////////////////////////////////////////////////////////////////////////////////////////////////
	//Recurring session
	/////////////////////////////////////////////////////////////////////////////////////////////////
	
	// Recurring session radio button
	@FindBy(xpath="/html/body/div/section/div/div/div[1]/div/div/div/div/div/div/form/div[3]/div/div[1]/div[2]/label/span[1]")
	public WebElement radio_RecurringSession;
	
	
	// Custom radio button
	@FindBy(xpath="/html/body/div/section/div/div/div[1]/div/div/div/div/div/div/form/div[3]/div/div[2]/div[2]/div/div[2]/div[1]/div[5]/label/span")
    public WebElement radio_Custom;
	
	// Custom session start date
	//@FindBy(name="customScheduleStartDate0")
	//public WebElement txt_CustomSessionStartDate;
	
	// For first field
	
	@FindBy(xpath="/html/body/div/section/div/div/div[1]/div/div/div/div/div/div/form/div[3]/div/div[2]/div[2]/div/div[2]/div[5]/div/div[1]/fieldset/div[1]/div/div/input")
	public WebElement txt_CustomSessionStartDate1;
	
		
	// Starts from icon 
	@FindBy(xpath="/html/body/div/section/div/div/div[1]/div/div/div/div/div/div/form/div[3]/div/div[2]/div[2]/div/div[2]/div[5]/div/div[1]/fieldset/div[2]/div/div/span/button")
	public WebElement icon_StartsFrom;
	
	// Custom time starts from
	@FindBy(name="customScheduleStartTime0")
	public WebElement txt_StartsFromTime; 
	
	
	// Add more button in custom 
	@FindBy(xpath="/html/body/div/section/div/div/div[1]/div/div/div/div/div/div/form/div[3]/div/div[2]/div[2]/div/div[2]/div[5]/div/div[2]/div/button")
	public WebElement btn_AddMore;
	
	// second start date text-box
	@FindBy(xpath="/html/body/div/section/div/div/div[1]/div/div/div/div/div/div/form/div[3]/div/div[2]/div[2]/div/div[2]/div[5]/div/div[1]/fieldset[2]/div[1]/div/div/input")
	public WebElement txt_StartDate2;
	
	// Second Start's from text-box
	@FindBy(xpath="/html/body/div/section/div/div/div[1]/div/div/div/div/div/div/form/div[3]/div/div[2]/div[2]/div/div[2]/div[5]/div/div[1]/fieldset[2]/div[2]/div/div/input")
	public WebElement txt_StartsFromTime2;
	
	
	// Third start date text-box
	@FindBy(xpath="/html/body/div/section/div/div/div[1]/div/div/div/div/div/div/form/div[3]/div/div[2]/div[2]/div/div[2]/div[5]/div/div[1]/fieldset[3]/div[1]/div/div/input")
	public WebElement txt_StartDate3;
	
	// Third Start's from text-box
	@FindBy(xpath="/html/body/div/section/div/div/div[1]/div/div/div/div/div/div/form/div[3]/div/div[2]/div[2]/div/div[2]/div[5]/div/div[1]/fieldset[3]/div[2]/div/div/input")
	public WebElement txt_StartsFromTime3;
	
	// Third start date text-box
	@FindBy(xpath="/html/body/div/section/div/div/div[1]/div/div/div/div/div/div/form/div[3]/div/div[2]/div[2]/div/div[2]/div[5]/div/div[1]/fieldset[4]/div[1]/div/div/input")
	public WebElement txt_StartDate4;
		
	// Third Start's from text-box
	@FindBy(xpath="/html/body/div/section/div/div/div[1]/div/div/div/div/div/div/form/div[3]/div/div[2]/div[2]/div/div[2]/div[5]/div/div[1]/fieldset[4]/div[2]/div/div/input")
	public WebElement txt_StartsFromTime4;
		
	// Custom time starts from
	@FindBy(id="price")
	public WebElement txt_SessionPrice; 
	
	@FindBy(xpath="/html/body/div/section/div/div/div[1]/div/div/div/div/div/div/form/div[8]/div/button[1]")
	public WebElement btn_PayNPublish;
			
	// Pay button in checkout page
	@FindBy(xpath="//*[@id='saved']/div/div/button")
	public WebElement btn_PayAmount;
	
}


