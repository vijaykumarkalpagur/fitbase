//Autored Krishna yadav Dasari on 05-08-2016

package com.fitbaseApp.modules.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class NewHomePage {

	public NewHomePage(WebDriver driver){
		PageFactory.initElements(driver, this);}

	@FindBy(linkText = "SIGN UP / LOG IN")     //div[@class='collapse navbar-collapse cust-nav wow fadeIn']/ul/li[@class='login']/a         
	@CacheLookup // keeps the element in cache for rapid execution
	public WebElement btnSignUpLogIn;

	@FindBy(xpath="//div[@class='modal-content']/div/div[@class='row']")              
	public WebElement modelView;

	@FindBy(xpath="//ul[@class='nav nav-tabs']//a[@id='tabsignup']")  //@FindBy(linkText = "Signup")                   
	@CacheLookup // keeps the element in cache for rapid execution
	public WebElement btnSignUp;

	@FindBy(xpath="//div[@class='modal-body']//ul[@class='nav nav-tabs']//a[@id='tablogin']")  //@FindBy(linkText = "Log in")              
	//@CacheLookup // keeps the element in cache for rapid execution
	public WebElement btnLogIn;


	//#######################################
	//Sign up:

	@FindBy(xpath="//form[@id='profile-create-form']")
	public WebElement formSignUp;

	@FindBy(xpath="//input[@id='firstName']")
	public WebElement txtboxFirstName;

	@FindBy(xpath="//input[@id='lastName']")
	public WebElement txtboxLastName;

	@FindBy(xpath="//input[@id='email']")
	public WebElement txtboxEmail;

	@FindBy(xpath="//form[@id='profile-create-form']//input[@id='signuppassword']")
	public WebElement txtboxPassword;

	@FindBy(xpath="//input[@id='terms']")
	public WebElement checkboxTerms;

	@FindBy(xpath="//input[@id='newsletters']")
	public WebElement checkboxNewsLetters;

	@FindBy(xpath="//form[@id='profile-create-form']//button")
	public WebElement btnSubmit;

	//#######################################
	//Login

	@FindBy(xpath="//form[@id='user-login-form']")
	public WebElement formLoginIN;

	@FindBy(xpath="//form[@id='user-login-form']//input[@id='username']")
	public WebElement txtboxUserName;

	@FindBy(xpath="//form[@id='user-login-form']//input[@id='username']/../div/span[@id='username-error']")
	public WebElement txtEnterValidEmailAddress;


	@FindBy(xpath="//form[@id='user-login-form']//input[@id='loginpassword']")
	public WebElement txtboxUserPassword;

	@FindBy(xpath="//form[@id='user-login-form']//input[@id='loginpassword']/..//div/span[@id='loginpassword-error']")
	public WebElement txtEnterValidPassword;

	@FindBy(xpath="//form[@id='user-login-form']//input[@name='remember-me']")
	public WebElement checkboxRememberMe;

	@FindBy(xpath="//form[@id='user-login-form']/div/div[@class='checkbox pull-left mzero']/label")
	public WebElement labelRemember;

	@FindBy(xpath="//form[@id='user-login-form']//span[@class='pull-right forget-pad']")
	public WebElement lnkForgotPassword;

	@FindBy(xpath="//form[@id='user-login-form']//button")
	public WebElement btnLogin;

	@FindBy(xpath="//form[@id='user-login-form']//div[@class='form-group']/label")   
	public WebElement labelMsg;

	@FindBy(xpath="//form[@id='user-login-form']//a[@id='signupid']")
	public WebElement lnkSignUp;

	@FindBy(xpath="//div[@id='login']//div[@class='modal-body']/button[@class='close']/span")
	public WebElement btnClose;

	@FindBy(xpath="//form[@id='user-login-form']//input[@id='username']/../div/span[@id='username-error']")
	public WebElement txtInvalidUNAndPWD;

	//########################################
	//Forgot password modal-body

	@FindBy(xpath="//h3[contains(text(),'FORGOT PASSWORD')]/../../..//div[@class='modal-body row']")
	public WebElement txtForgotPasswordModalBody;

	@FindBy(xpath="//h3[contains(text(),'FORGOT PASSWORD')]/../../..//div[@class='modal-body row']/div/h3")
	public WebElement txtForgotPassword;

	@FindBy(xpath="//h3[contains(text(),'FORGOT PASSWORD')]/../../..//div[@class='modal-body row']/div/p[1]")
	public WebElement txtInfAboutResetYourPassword;

	@FindBy(xpath="//h3[contains(text(),'FORGOT PASSWORD')]/../../..//div[@class='modal-body row']/div/button")
	public WebElement btnCloseOfResetPasswordModalBody;

	@FindBy(xpath="//h3[contains(text(),'FORGOT PASSWORD')]/../../..//div[@class='modal-body row']/div/form[@id='resetForm']//input")
	public WebElement txtBoxEmailIdForResetPassword;

	@FindBy(xpath="//h3[contains(text(),'FORGOT PASSWORD')]/../../..//div[@class='modal-body row']/div/form[@id='resetForm']/button")
	public WebElement btnResetPassword;

	@FindBy(xpath="//h3[contains(text(),'FORGOT PASSWORD')]/../../..//div[@class='modal-body row']/div/p[2]")
	public WebElement txtInfAboutBackToLogin;

	@FindBy(xpath="//h3[contains(text(),'FORGOT PASSWORD')]/../../..//div[@class='modal-body row']/div/p[2]/a")
	public WebElement lnkBackToLogin;

	@FindBy(xpath="//h3[contains(text(),'FORGOT PASSWORD')]/../../..//div[@class='modal-body row']//span[@id='resetpassword-success']")
	public WebElement msgResetPasswordSuccess;

	@FindBy(xpath="//h3[contains(text(),'FORGOT PASSWORD')]/../../..//div[@class='modal-body row']//span[@id='resetpassword-error']")
	public WebElement msgResetPasswordError;

	//########################################
	//Facebook:

	@FindBy(xpath="//div[@id='login']/div[@class='modal-dialog']/div[@class='modal-content']/div/div/div[1]/div/div[1]/a/i")              
	//@CacheLookup // keeps the element in cache for rapid execution
	public WebElement btnfacebook;

	@FindBy(xpath = ".//*[@id='email']")
	public WebElement txtFacebookUserName;

	@FindBy(xpath = ".//*[@id='pass']")
	public WebElement txtFacebookPassword;

	@FindBy(xpath = ".//*[@id='loginbutton']")
	public WebElement btnFacebookLogin;

	@FindBy(xpath = "//form[@id='login_form']/div[@role='alert']/div[1]")
	public WebElement IncorrectEmailDiv;

	//########################################
	//Google-gmail: 

	@FindBy(xpath="//div[@id='login']/div[@class='modal-dialog']/div[@class='modal-content']/div/div/div[1]/div/div[2]/a/i")              
	//@CacheLookup // keeps the element in cache for rapid execution
	public WebElement btngooglePlus;

	@FindBy(xpath = "//div[@role='presentation']//input[@type='email']")
	public WebElement txtGmailUserName;

	@FindBy(xpath = ".//*[@id='identifierNext']")
	public WebElement btnGmailNext;

	@FindBy(xpath = "//div[@id='password']/div[1]/div/div[1]/input[@name='password']")
	public WebElement txtGmailPassword;

	@FindBy(xpath = ".//*[@id='passwordNext']")
	public WebElement btnGmailLogin;              //   Gmail Password Next button

	@FindBy(xpath = "//button[@id='submit_approve_access']")
	public WebElement btnAllow;

	//Logout From HomePage
	@FindBy(xpath = "//nav[@role='navigation']//div[@class='collapse navbar-collapse cust-nav wow fadeIn']/ul/li[@id='loggedinuser']/a/span[2]")
	public WebElement loginUser;

//	@FindBy(xpath = "//nav[@role='navigation']//div[@class='collapse navbar-collapse cust-nav wow fadeIn']/ul/li[@id='loggedinuser']/a/span[2]/../../ul/li[9]/a")
//	public WebElement logoutFromHomePage;
	
	@FindBy(xpath = "//li[@id='loggedinuser']/a/span[2]/../../ul/li[9]/a[@href='/app/logout']")
	public WebElement logoutFromHomePage;

	@FindBy(xpath = "//div[@id='gb']//div[@class='gb_ic gb_wf gb_R']//a[@class='gb_b gb_db gb_R']/span")
	public WebElement btnGmailMore;

	@FindBy(xpath = "//div[@id='gb']//div[@class='gb_ic gb_wf gb_R']//a[@class='gb_b gb_db gb_R']/span/../../..//a[@class='gb_Fa gb_He gb_Oe gb_wb']")
	public WebElement btnGmailSignOut;


	//#################################
	//Fitbase email verification successes page 


	@FindBy(xpath = "//div[@class='container']//h1[@class='customheader']")
	public WebElement txtCongratulations;

	@FindBy(xpath = "//div[@class='container']//h1[@class='customheader']/../div/span[@class='verified']/i")
	public WebElement imgVerified;	

	@FindBy(xpath = "//div[@class='container']//h1[@class='customheader']/../div/p")
	public WebElement msgEmailVerified;

	@FindBy(xpath = "//div[@class='container']//h1[@class='customheader']/../div/a/span")
	public WebElement lnkGoToDashboard;



}