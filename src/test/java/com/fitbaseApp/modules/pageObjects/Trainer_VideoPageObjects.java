package com.fitbaseApp.modules.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Trainer_VideoPageObjects {

	public Trainer_VideoPageObjects(WebDriver driver){
		PageFactory.initElements(driver, this);
		} 

	
	@FindBy(xpath="/html/body/div/nav/div[1]/div/div") 
	public WebElement header_liveSession;
	
	@FindBy(xpath="//button[@id='livebutton']")
	public WebElement btn_LiveSession;
	
	@FindBy(xpath="//div[@id='shareInfo' and @class='ng-scope']//p[contains(text(),'Waiting for user to join')]")
	public WebElement msg_WaitingForUser;
}
