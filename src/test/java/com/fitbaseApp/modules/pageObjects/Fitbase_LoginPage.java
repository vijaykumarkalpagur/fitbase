package com.fitbaseApp.modules.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Fitbase_LoginPage {

	public Fitbase_LoginPage(WebDriver driver){
		PageFactory.initElements(driver, this);}

	
	@FindBy(linkText="SIGN UP / LOG IN")              
	public WebElement btnMainLogin;
	
	//Normal login page objects
	@FindBy(name="username")
	public WebElement txt_username;
	
	@FindBy(xpath="//input[@id='loginpassword' and @name='password']")
	public WebElement txt_password;
	
	@FindBy(xpath="//button[@id='login-form']")
	public WebElement btnLoginPopUp;
	
	
	// Gmail button
	@FindBy(xpath=".//*[@id='sign-in-google']")
	public WebElement googleBtn;
			
	// FACEBOOK
	@FindBy(xpath = ".//*[@id='sign-in-fb']")
	public WebElement btn_facebook;
		   	
	// @FindBy(xpath = ".//*[@id='email']")
	@FindBy(id = "email")
	public WebElement txt_facebookUserName;
		   	
	//	@FindBy(xpath = ".//*[@id='pass']")
	@FindBy(id = "pass")
	public WebElement txt_facebookPassword;
		   	
	@FindBy(name = "login")
	public WebElement btn_facebookLogin;
		   	
	//GMAIL
		   	
	@FindBy(xpath = ".//*[@id='sign-in-google']")
	public WebElement btn_gmail;
		   	
	//@FindBy(xpath = ".//*[@id='Email']")
	@FindBy(name="identifier")
	public WebElement txt_gmailUserName;
		   	
	@FindBy(xpath = "//Content[@class='CwaK9']//span[contains(text(),'Next')]")
	public WebElement btn_gmailNext;
		   	
	@FindBy(xpath = "//input[@class='whsOnd zHQkBf']")
	public WebElement txt_gmailPassword;
		   	
	@FindBy(xpath = "//span[@class='RveJvd snByac']")
	public WebElement btn_gmailLogin;
	
	// Logout
	
	@FindBy(xpath="/html/body/nav/div/div/div/div/div[1]/ul/li/a")
	public WebElement drp_menu;
	
	@FindBy(xpath="/html/body/nav/div/div/div/div/div[1]/ul/li/ul/li[7]/a")
	public WebElement lnk_Logout;
		   	
}
