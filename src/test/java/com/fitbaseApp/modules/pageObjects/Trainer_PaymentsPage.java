package com.fitbaseApp.modules.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Trainer_PaymentsPage {
	
	public Trainer_PaymentsPage(WebDriver driver){
		PageFactory.initElements(driver, this);} 

	
	// Stripe payment elements
	@FindBy(xpath="html/body/div[1]/section/div/div/section/div/div/div[2]/div/div/div[3]/ul/li[1]/button") 
	public WebElement savedCardsTab;
	
	//@FindBy(xpath="//*[@id='saved']/div/div/button")
	@FindBy(xpath="//button[@type='submit' and @ng-click='chargeCard(selectedCard, customerId)']")
	public WebElement btn_StripePayNow;
	
	//@FindBy(xpath="//div[@id='paymentDisable']/ul/li[2]/button")
	//@FindBy(xpath="//*[@id='paymentDisable']/ul/li[2]/button")
	//@FindBy(xpath="//button[contains(text(),'Credit/Debit Card')]")
	
	//@FindBy(xpath="//ul[@class='nav nav-tabs']//li[2]")
	@FindBy(xpath="//div[@class = 'fb-pay-opt' and @id ='paymentDisable']//ul[@class = 'nav nav-tabs']//li[2]//button")
	public WebElement creditNdebitcardTab;
	

//	@FindBy(xpath ="//*[@id='root']/form/div/div[2]/span[1]/span[2]/label/input")
//	@FindBy(xpath ="//input[@placeholder='Card number' and @name='cardnumber']")
	
	//@FindBy(xpath ="//label[@class='Input']")
	//@FindBy(xpath="//div[@id= 'root']//div[@class='CardField-input-wrapper is-ready-to-slide']//span[1]//label[@class='Input']//input[@name ='cardnumber']")
	@FindBy(xpath="//*[@id='root']//form//div//div//span//span//label//input[@type='tel' and @name='cardnumber']")
//	@FindBy(xpath="//*[@id='root']/form/div/div[2]/span[1]/span[2]/label/input")
	//@FindBy(xpath="//div//div//div//span//span//label//input[@type='tel' and @autocomplete='cc-number' and @autocorrect='off' and @spellcheck='false' and @name='cardnumber']")
	//@FindBy(xpath="//input[@type='tel' and @autocomplete='cc-number' and @autocorrect='off' and @spellcheck='false' and @name='cardnumber']")
	public WebElement txt_CardNumber;
	
	@FindBy(name="exp-date")
	public WebElement txt_MonthNYear;
	
	@FindBy(name="cvc")
	public WebElement txt_CVC;
	
	
	// Paypal payment elements
	@FindBy(xpath="//div[@class = 'fb-pay-opt' and @id ='paymentDisable']//ul[@class = 'nav nav-tabs']//li[3]//button")
	public WebElement paypalTab;
	
	@FindBy(xpath="//button[@type= 'submit' and @class='pay']")
	public WebElement btn_PaypalPayNow;
	
	// Payapal sandbox elements
	@FindBy(linkText="Log In")
	public WebElement lnk_payPalLogin;
	
	@FindBy(name="login_email")
	public WebElement txt_PaypalLoginEmail;
	
	@FindBy(name="btnNext")
	public WebElement btn_PaypalNext;
	
	@FindBy(name="login_password")
	public WebElement txt_PaypalLoginPassword;
	
	@FindBy(name="btnLogin")
	public WebElement btn_PaypalLogin;
	
	@FindBy(xpath="//input[@type='submit' and @value='Continue' and @id='confirmButtonTop' and @validate-submit='onPay()']")
	public WebElement btn_PaypalContinuePayment;
	
}
