package com.fitbaseApp.modules.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class Fitbase_LiveSessionsPageObjects {

	public Fitbase_LiveSessionsPageObjects(WebDriver driver){
		PageFactory.initElements(driver, this);
	}

// Live sessions main link
@FindBy(xpath="//a[@href='/app/#/livesessions/workoutsessions']")
public WebElement lnk_LiveSessionModule;

//Live session module page objects	
//@FindBy(xpath="//h4[contains(text(), 'more days')]")
@FindBy(xpath="//*[@id='schedulewkt-list-wrapper']/div[1]/a/div[2]/div[2]/h4")
public WebElement lnk_LiveSessionsName;
	
@FindBy(xpath="//button[contains(text(), 'Enroll Now')]")
public WebElement btn_EnrollNow;

@FindBy(xpath="//button[@ng-click='closeConflictModel();openPaymentModel()']")
public WebElement btn_ConflictPopUp;

@FindBy(xpath="//button[@ng-click='payNow(lookup)']")
public WebElement btn_EnrollNowbtnInPopUp;

//Edited By Vijay

@FindBy(how=How.XPATH,using = "//*[@id='paymentDisable']/ul/li[3]/button")
public WebElement ClickonPayPalTab;

@FindBy(how=How.XPATH,using = "//*[@id='paypal']/div/div/button")
public WebElement ContinuetoPayPalButton;

@FindBy(how=How.XPATH,using = "//*[@id='loginSection']/div/div[2]/a")
public WebElement PayPalLoginButton;

@FindBy(how=How.XPATH,using = "//*[@id='email']")
public WebElement EnterPayPalEmailID;

@FindBy(how=How.XPATH,using = "//*[@id='btnNext']")
public WebElement ClickOnNextButton;

@FindBy(how=How.XPATH,using = "//*[@id='password']")
public WebElement EnterPayPalPassword;

@FindBy(how=How.XPATH,using = "//*[@id='btnLogin']")
public WebElement ClickOnLoginButton;

@FindBy(how=How.XPATH,using = "//*[@id='confirmButtonTop']")
public WebElement ClickonconfirmButton;



//***********************************************************
// Select Free sessions from drop-down
@FindBy(xpath="//select[@id='Price']//option[@value='Free']")
public WebElement value_FreeSessions;

@FindBy(xpath="//select[@id='Price']//option[@value='1']")
public WebElement value_PaidSessions;

//***********************************************************
//Sort by type 1-ON-1 OR Group
@FindBy(xpath="//select[@id='level']//option[@value='OneToOneWorkout']")
public WebElement value_1_ON_1;

@FindBy(xpath="//select[@id='level']//option[@value='Group']")
public WebElement value_Group;

//***********************************************************
// Select Expert level

@FindBy(xpath="//select[@id='level']//option[@value='Expert']")
public WebElement value_ExpertLevel;

//***********************************************************
// Saved cards
@FindBy(xpath="//a[@data-target='#savedCards']")
public WebElement tab_SavedCards;

@FindBy(xpath="//button[@type='submit']")
public WebElement btn_StripePayNow;

//***********************************************************
// Add new card

// @FindBy(xpath="//div[@class = 'fb-pay-opt' and @id ='paymentDisable']//ul[@class = 'nav nav-tabs']//li[2]//a")
@FindBy(xpath="//a[contains(text(), 'Credit/Debit Card')]")
public WebElement tab_CreditORdebitCard;

@FindBy(xpath="//div[@id='card-element']")
public WebElement txt_CardNumber;

@FindBy(how=How.XPATH,using = "//div[@id='card-element']//iframe[1]")
public WebElement EnterCardNumber;

@FindBy(how=How.XPATH,using = "//*[@id='card']/form/div[2]/button")
public WebElement ClickonPayButton;

//***********************************************************
//PayPal
//Paypal payment elements
	@FindBy(xpath="//div[@class = 'fb-pay-opt' and @id ='paymentDisable']//ul[@class = 'nav nav-tabs']//li[3]//button")
	public WebElement paypalTab;
	
	@FindBy(xpath="//*[@id='paypal']/div/button")
	public WebElement btn_PaypalPayNow;
	
	// Payapal sandbox elements
	@FindBy(linkText="Log In")
	public WebElement lnk_payPalLogin;
		
	@FindBy(how=How.XPATH,using = "//*[@id='email']")
	public WebElement txt_PaypalLoginEmail;
	
	@FindBy(name="btnNext")
	public WebElement btn_PaypalNext;
	
	@FindBy(name="login_password")
	public WebElement txt_PaypalLoginPassword;
	
	@FindBy(name="btnLogin")
	public WebElement btn_PaypalLogin;
	
	@FindBy(xpath="//input[@type='submit' and @value='Continue' and @id='confirmButtonTop' and @validate-submit='onPay()']")
	public WebElement btn_PaypalContinuePayment;
	
	
}
