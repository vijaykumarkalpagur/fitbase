package com.fitbaseApp.modules.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Fitbase_DevicesPage {
	
		public Fitbase_DevicesPage(WebDriver driver){
			PageFactory.initElements(driver, this);
		}
	
	// Devices main link
	@FindBy(xpath="//li//a[@ng-href='/app/#/device']")
	public WebElement lnk_devices;
	
	//Fitbit	
	@FindBy(xpath="//button[@ng-hide='activateFitbit']")
	public WebElement btn_connectFitbit;
		
	@FindBy(xpath="//input[@name='email' and @tabindex='23']")
	public WebElement txt_fitbitEmail;
	
	@FindBy(xpath="//input[@name='password' and @tabindex='24']")
	public WebElement txt_fitbitPassword;
	
	@FindBy(xpath="//*[@id='loginForm']/div[1]/button")
	public WebElement btn_FitbitLogin;	
	
	@FindBy(xpath="//input[@id='selectAllScope']")
	public WebElement checkbox_AllowAll;
	
	@FindBy(xpath="//button[@id='allow-button']")
	public WebElement btn_FitbitAllow;
	
	@FindBy(xpath="//button[@ng-show='syncFitbit']")					
	public WebElement btn_syncFitbitData;
		
	@FindBy(xpath="//span[@ng-show='showFitbitLastSyncTime']")
	public WebElement msg_FitbitLastSync;
	
	
	/*// Jawbone
		
	@FindBy(xpath="//button[@ng-hide='activateFitbit']")
	public WebElement btn_connectJawbone;
		
	@FindBy(xpath="//input[@name='email' and @tabindex='23']")
	public WebElement txt_jawboneEmail;
	
	@FindBy(xpath="//input[@name='password' and @tabindex='24']")
	public WebElement txt_jawbonePassword;
	
	@FindBy(xpath="//*[@id='loginForm']/div[1]/button")
	public WebElement btn_jawboneLogin;	
	*/
	
	
	
	// MISFIT
	
	@FindBy(xpath="//button[@ng-hide='activateMisfit']")
	public WebElement btn_connectMisfit;
	
	@FindBy(xpath="//input[@type='email' and @name='email']")
	public WebElement txt_MisfitEmail;
	
	@FindBy(xpath="//input[@type='password' and @name='password']")
	public WebElement txt_MisfitPassword;
	
	@FindBy(xpath="//button[@id='login-btn' and @type='submit']")
	public WebElement btn_MisfitLogin;	
	
	@FindBy(xpath="//button[@ng-show='syncMisfit']")					
	public WebElement btn_syncMisfitData;
		
	@FindBy(xpath="//span[@ng-show='showMisfitLastSyncTime']")
	public WebElement msg_MisfitLastSync;
	
	
	// Nokia
	
	
	@FindBy(xpath="//button[@ng-hide='activateWithings']")
	public WebElement btn_connectNokia;
		
	@FindBy(xpath="//input[@type='email' and @name='email']")
	public WebElement txt_NokiaEmail;
		
	@FindBy(xpath="//input[@type='password' and @name='password']")
	public WebElement txt_NokiaPassword;
		
	@FindBy(xpath="//button[@type='submit']")
	public WebElement btn_NokiaLogin;	
		
	@FindBy(xpath="//input[@type='button' and @value='Allow']")
	public WebElement btn_NokiaAllow;
	
	@FindBy(xpath="//button[@ng-show='syncWithings']")					
	public WebElement btn_syncNokiaData;
			
	@FindBy(xpath="//span[@ng-show='showWithingsLastSyncTime']")
	public WebElement msg_NokiaLastSync;
		
	
	// Strava
	
	
		@FindBy(xpath="//button[@ng-hide='activateStrava']")
		public WebElement btn_connectStrava;
			
		@FindBy(xpath="//input[@type='email' and @name='email' and @id='email']")
		public WebElement txt_StravaEmail;
			
		@FindBy(xpath="//input[@type='password' and @name='password']")
		public WebElement txt_StravaPassword;
		
		@FindBy(xpath="//button[@class='btn-dismiss-cookie-banner']")
		public WebElement btn_StravaCookie;
		
		@FindBy(xpath="//button[@type='submit']")
		public WebElement btn_StravaLogin;	
			
		@FindBy(xpath="//button[contains(text(), 'Authorize')]")
		public WebElement btn_StravaAuthorize;
		
		@FindBy(xpath="//button[@ng-show='syncStrava']")					
		public WebElement btn_syncStravaData;
				
		@FindBy(xpath="//span[@ng-show='showStravaLastSyncTime']")
		public WebElement msg_StravaLastSync;
		
		// Polar
			
			@FindBy(xpath="//button[@ng-hide='activatePolar']")
			public WebElement btn_connectPolar;
				
			@FindBy(xpath="//input[@type='email' and @name='email' and @id='email']")
			public WebElement txt_PolarEmail;
				
			@FindBy(xpath="//input[@type='password' and @name='password']")
			public WebElement txt_PolarPassword;
			
			@FindBy(xpath="//button[@type='submit']")
			public WebElement btn_PolarLogin;	
				
			@FindBy(xpath="//button[contains(text(), 'Authorize')]")
			public WebElement btn_PolarAuthorize;
			
			@FindBy(xpath="//button[@ng-show='syncPolar']")					
			public WebElement btn_syncPolarData;
					
			@FindBy(xpath="//span[@ng-show='showPolarLastSyncTime']")
			public WebElement msg_PolarLastSync;
	
	
	}

