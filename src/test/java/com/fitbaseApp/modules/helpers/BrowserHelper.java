package com.fitbaseApp.modules.helpers;

import java.awt.Toolkit;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.fitbaseApp.modules.pageObjects.NewHomePage;
import com.fitbaseApp.utilities.Constant;
import com.fitbaseApp.utilities.Log;
import com.google.common.base.Function;


public class BrowserHelper {

	public static WebDriver driver = null;
	public static WebDriver getDriver() {
		return driver;}
	//#################################################################################
	public static WebDriver openBrowser(String sBrowserName) throws Exception{
		try{
			if (sBrowserName.equalsIgnoreCase("firefox")){
				System.setProperty("webdriver.gecko.driver", ".\\geckodriver.exe");
				driver = new FirefoxDriver();
				Log.info("Driver instantiated");
				driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				driver.get(Constant.URL);
				driver.manage().window().maximize();
				//maximise();
				//maximizeWindow();
				Log.info("Web application launched successfully.");}
			else if(sBrowserName.equalsIgnoreCase("chrome")){
				System.setProperty("webdriver.chrome.driver", ".\\chromedriver.exe");
				driver = new ChromeDriver();
				Log.info("Driver instantiated");
				
				driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				driver.get(Constant.URL);
				driver.manage().window().maximize();
				Log.info("Web application launched successfully.");}
			else if(sBrowserName.equalsIgnoreCase("ie")){
				System.setProperty("webdriver.ie.driver",".\\IEDriverServer.exe");
				driver = new InternetExplorerDriver();
				Log.info("Driver instantiated");
				driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				driver.get(Constant.URL);
				driver.manage().window().maximize();
				Log.info("Web application launched successfully.");}
		}
		catch (Exception e){
			Log.error("class Utils | Method OpenBrowser | Exception desc : " + e.getMessage());}
		return driver;
	}
	//#################################################################################

	public static String getTestCaseName(String sTestCase) throws Exception{
		String value = sTestCase;
		try{
			int posi = value.indexOf("@");
			value  = value.substring(0, posi);
			posi = value.lastIndexOf(".");
			value = value.substring(posi + 1);
			return value;
		}catch (Exception e){
			Log.error("Class Utils | Method getTestCaseName | Exception desc : " + e.getMessage());
			throw(e);}
	}
	//#################################################################################
	//fluent wait for to get element : Waiting 30 seconds for an element to be present on the page, checking for its presence once every 5 seconds.
	public static void getElementByLocator(final By locator) { 
		//Declaring a fluent wait
		@SuppressWarnings("deprecation")
		Wait<WebDriver> wait1 = new FluentWait<WebDriver>(driver).withTimeout(30, TimeUnit.SECONDS).pollingEvery(5, TimeUnit.SECONDS).ignoring(NoSuchElementException.class);
		//identifying web element on the page
		wait1.until(new Function<WebDriver, WebElement>() {
			public WebElement apply(WebDriver driver) {return driver.findElement(locator);}});
	}

	//#################################################################################
	//Explicit Wait for element load
	public static void waitElementByLocator (final By locator,int numberOfSeconds) {
		new WebDriverWait(driver, numberOfSeconds).until( ExpectedConditions.presenceOfElementLocated(locator) ); 
	}

	//#################################################################################
	//verifying element is not existing: before this method use Thread.sleep() for page load or any displayed element on the page.
	public static void ElementNotDisplayed(final By locator) {
		//Log.info("element:::::::::"+driver.findElement(locator).getText());
		Assert.assertTrue(driver.findElement(locator).getText().isEmpty());
		Log.info("Success:Element not displayed...");
	}

	//#################################################################################
	//verifying element is existing
	public static void ElementIsDisplayed(final By locator,int numberOfSeconds) {
		new WebDriverWait(driver, numberOfSeconds).until( ExpectedConditions.presenceOfElementLocated(locator) ); 
		Assert.assertTrue(driver.findElement(locator).isDisplayed());
		Log.info("Success:Element is displayed...");
	}


	//#################################################################################
	//Verifying user list count in page
	public static void userListCountInPage(final By locator,int expectedCount) {

		List<WebElement> userNames = driver.findElements(locator);
		Log.info("Number of users:"+userNames.size());    
		String[] nameTexts = new String[userNames.size()];
		int i = 0;
		for (WebElement e : userNames) {
			nameTexts[i] = e.getText();
			i++;}
		for (String t : nameTexts) {Log.info(t);}
		Assert.assertEquals(userNames.size(), expectedCount);

	}

	//#################################################################################

	//Mouse over the element and click
	public static void mouseHover(final By locator1,final By locator2) {

		Actions builder = new Actions(driver);
		Action mouseOverFriend = builder
				.moveToElement(driver.findElement(locator1))
				.click()
				.moveToElement(driver.findElement(locator2))
				.click()
				.build();
		mouseOverFriend.perform();
	}

	//#################################################################################


	//fluent wait for to get element : Waiting 30 seconds for an element to be present on the page, checking for its presence once every 5 seconds.
	public static void getElementByLocatorByRefreshing(final By locator) { 
		//Declaring a fluent wait
		@SuppressWarnings("deprecation")
		Wait<WebDriver> wait1 = new FluentWait<WebDriver>(driver).withTimeout(30, TimeUnit.SECONDS).pollingEvery(5, TimeUnit.SECONDS).ignoring(NoSuchElementException.class);
		//identifying web element on the page
		wait1.until(new Function<WebDriver, WebElement>() {
			public WebElement apply(WebDriver driver) {
				driver.navigate().refresh();
				return driver.findElement(locator);}});
	}
	//#################################################################################

	//Mouse over the element and click
	public static void closeLoginUserFromHomePage() throws Exception{
		NewHomePage newHomePage = new NewHomePage(driver);

		Thread.sleep(3000);
		try{
			if(newHomePage.loginUser.isDisplayed())
			{
				newHomePage.loginUser.click();
				Thread.sleep(3000);
				newHomePage.logoutFromHomePage.isDisplayed();
				newHomePage.logoutFromHomePage.click();
				Thread.sleep(3000);
				BrowserHelper.ElementIsDisplayed(By.xpath("//nav[@role='navigation']//div[@class='collapse navbar-collapse cust-nav']/ul/li[@id='signuplogin']/a"), 5);
				Thread.sleep(3000);
			}
		}
		catch(Exception e)
		{
			Log.info("Successfully user has logedout ******");
		}
	}


	public static void maximise() { 
		Toolkit toolkit = Toolkit.getDefaultToolkit();

		Dimension screenResolution = new Dimension((int)toolkit.getScreenSize().getWidth(), (int)toolkit.getScreenSize().getHeight());
		driver.manage().window().setSize(screenResolution);

	}

	public static String maximizeWindow() {
		return "if (window.screen) {window.moveTo(0, 0);window.resizeTo(window.screen.availWidth,window.screen.availHeight)}";
	}


	//#################################################################################
	//click element by using java script
	public static void clickElement(final By locator) throws Exception{

		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].click();", driver.findElement(locator));
		Thread.sleep(3000);
	}




}

