package com.fitbaseApp.modules.actions;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import com.trainer.qa.module.helpers.CreateScheduleshelper;
import com.trainer.qa.module.pages.CreateSingleSessionPage;
import com.trainer.qa.module.pages.SessionsPaymentPages;
import com.trainer.qa.utility.Constant;
import com.trainer.qa.utility.Log;

public class SessionPaymentAction {

	static WebDriver driver;
	
	public static void SessionPaymentwithCard(WebDriver driver) throws Exception{
		CreateSingleSessionPage singlesession = new CreateSingleSessionPage(driver);
		SessionsPaymentPages SessionPayment = new SessionsPaymentPages(driver);
		
		    singlesession.CreateScheduleButton.isDisplayed();
			singlesession.CreateScheduleButton.isEnabled();
			singlesession.CreateScheduleButton.click();
		    Thread.sleep(2000);
			Log.info("Click action performed on CreateScheduleButton.");
			
			singlesession.NameoftheWorkoutSession.isDisplayed();
			singlesession.NameoftheWorkoutSession.clear();
			singlesession.NameoftheWorkoutSession.sendKeys(CreateScheduleshelper.singleworkoutsession());
		    Thread.sleep(1000);
			Log.info("Entered name of the workoutsession");
			
			singlesession.TypeofWorkout.isDisplayed();
			singlesession.TypeofWorkout.click();
			singlesession.TypeofWorkout.sendKeys(CreateScheduleshelper.TypeofWorkout());
			Log.info("Session TypeofWorkout is selected");
			
			singlesession.ClickonGroupSize.click();
			singlesession.GroupSize.sendKeys(CreateScheduleshelper.GroupSession());
			Log.info("Group Session size is selected");
			
			singlesession.WorkoutActivity.isDisplayed();
			singlesession.WorkoutActivity.click();
			
			singlesession.WorkoutAcvtivitySelection.clear();
			singlesession.WorkoutAcvtivitySelection.sendKeys(CreateScheduleshelper.WorkoutActivity());
			Thread.sleep(3000);
			
			singlesession.ClickonWorkoutAcvtivity.click();
		    Thread.sleep(1000);
			Log.info("Session WorkoutActivity is selected");
			
			singlesession.TrainingLevel.isDisplayed();
			singlesession.TrainingLevel.click();
			singlesession.TrainingLevel.sendKeys(CreateScheduleshelper.TrainingLevel());
		    Thread.sleep(1000);
			Log.info("Session TrainingLevel is selected");
			
			singlesession.SessionDate.isDisplayed();
			singlesession.SessionDate.click();
			singlesession.SessionDate.sendKeys(CreateScheduleshelper.AddDatestocurrentSystemDate("mm/dd/yyyy",Constant.StartDate));
			Thread.sleep(2000);
			Log.info("Session Date is selected");
			
			singlesession.SessionTime.isDisplayed();
			singlesession.SessionTime.clear();
			singlesession.SessionTime.sendKeys(CreateScheduleshelper.randomTimeGenerate("hh:mm a"));
			Log.info("Session time is selected");
			
			singlesession.SessionDuration.isDisplayed();
			singlesession.SessionDuration.click();
			singlesession.SessionDuration.sendKeys(CreateScheduleshelper.Duration("Minutes"));
			Log.info("Session Duration is selected");
			Thread.sleep(3000);
			
			Actions action = new Actions(driver);
			action.sendKeys(Keys.PAGE_DOWN).build().perform();
			
			SessionPayment.SessionPrice.clear();
			Thread.sleep(1000);
			SessionPayment.SessionPrice.sendKeys("0");	
			
			Thread.sleep(1000);
			singlesession.PublishButton.isDisplayed();
			singlesession.PublishButton.isEnabled();
			singlesession.PublishButton.click();
		    Thread.sleep(4000);
			Log.info("Click action performed on Publish Button.");
			
			SessionPayment.CreditorDebitCard.click();
			Log.info("Selected CreditorDebitCard Tab");

			SessionPayment.EnterCardNumber.sendKeys("42424242424242424242424");
			Log.info("Enter Valid Credit/DebitCard Number");
     		Thread.sleep(3000);
     		
     		SessionPayment.ClickonPayButton.click();
     		Thread.sleep(15000);
    		Log.info("Clicked on PayNow Button");
     		
     		SessionPayment.ClickonPreviewSessionButton.click();
     		Thread.sleep(9000);
     		
			singlesession.DeleteWorkoutSession.click();
			Thread.sleep(1000);
			
			singlesession.ConfirmDeleteWorkoutSession.click();
			Thread.sleep(2000);	
		}	

	public static void PaymentwithPayPal(WebDriver driver) throws Exception{
		CreateSingleSessionPage singlesession = new CreateSingleSessionPage(driver);
		SessionsPaymentPages SessionPayment = new SessionsPaymentPages(driver);
		
		    singlesession.CreateScheduleButton.isDisplayed();
			singlesession.CreateScheduleButton.isEnabled();
			singlesession.CreateScheduleButton.click();
		    Thread.sleep(2000);
			Log.info("Click action performed on CreateScheduleButton.");
			
			singlesession.NameoftheWorkoutSession.isDisplayed();
			singlesession.NameoftheWorkoutSession.clear();
			singlesession.NameoftheWorkoutSession.sendKeys(CreateScheduleshelper.singleworkoutsession());
		    Thread.sleep(1000);
			Log.info("Entered name of the workoutsession");
			
			singlesession.TypeofWorkout.isDisplayed();
			singlesession.TypeofWorkout.click();
			singlesession.TypeofWorkout.sendKeys(CreateScheduleshelper.TypeofWorkout());
			Log.info("Session TypeofWorkout is selected");
			
			singlesession.ClickonGroupSize.click();
			singlesession.GroupSize.sendKeys(CreateScheduleshelper.GroupSession());
			Log.info("Group Session size is selected");
			
			singlesession.WorkoutActivity.isDisplayed();
			singlesession.WorkoutActivity.click();
			
			singlesession.WorkoutAcvtivitySelection.clear();
			singlesession.WorkoutAcvtivitySelection.sendKeys(CreateScheduleshelper.WorkoutActivity());
			/*Thread.sleep(3000);*/
			
			singlesession.ClickonWorkoutAcvtivity.click();
		    Thread.sleep(1000);
			Log.info("Session WorkoutActivity is selected");
			
			singlesession.TrainingLevel.isDisplayed();
			singlesession.TrainingLevel.click();
			singlesession.TrainingLevel.sendKeys(CreateScheduleshelper.TrainingLevel());
		    Thread.sleep(1000);
			Log.info("Session TrainingLevel is selected");
			
			singlesession.SessionDate.isDisplayed();
			singlesession.SessionDate.click();
			singlesession.SessionDate.sendKeys(CreateScheduleshelper.AddDatestocurrentSystemDate("mm/dd/yyyy",Constant.StartDate));
			Thread.sleep(2000);
			Log.info("Session Date is selected");
			
			singlesession.SessionTime.isDisplayed();
			singlesession.SessionTime.clear();
			singlesession.SessionTime.sendKeys(CreateScheduleshelper.randomTimeGenerate("hh:mm a"));
			Log.info("Session time is selected");
			
			singlesession.SessionDuration.isDisplayed();
			singlesession.SessionDuration.click();
			singlesession.SessionDuration.sendKeys(CreateScheduleshelper.Duration("Minutes"));
			Log.info("Session Duration is selected");
			Thread.sleep(3000);
			
			Actions action = new Actions(driver);
			action.sendKeys(Keys.PAGE_DOWN).build().perform();
			
			SessionPayment.SessionPrice.clear();
			Thread.sleep(1000);
			SessionPayment.SessionPrice.sendKeys("0");	
			Log.info("Session Price Entered");
			
			Thread.sleep(1000);
			singlesession.PublishButton.isDisplayed();
			singlesession.PublishButton.isEnabled();
			singlesession.PublishButton.click();
		    Thread.sleep(4000);
			Log.info("Clicked on Publish Button.");
			
			SessionPayment.ClickonPayPalTab.click();
			Log.info("Session time is selected");
			
			action.sendKeys(Keys.PAGE_DOWN).build().perform();
			Thread.sleep(1500);
			SessionPayment.ContinuetoPayPalButton.click();
			Thread.sleep(5000);
			
//			SessionPayment.PayPalLoginButton.click();
//			Thread.sleep(5000);
			
			SessionPayment.EnterPayPalEmailID.sendKeys("vijaykumarkalpagur@gmail.com");
			SessionPayment.ClickOnNextButton.click();
			Thread.sleep(2000);
			Log.info("Entered PayPal valid Email ID");
			
			SessionPayment.EnterPayPalPassword.sendKeys("Testing@123");
			SessionPayment.ClickOnLoginButton.click();
			Thread.sleep(4000);
			Log.info("Entered PayPal valid Password");
			Log.info("Click on PayPal Account Login Button");
			SessionPayment.ClickonconfirmButton.click();

     		Thread.sleep(15000);
     		SessionPayment.ClickonPreviewSessionButton.click();
     		Thread.sleep(9000);
     		
			singlesession.DeleteWorkoutSession.click();
			Thread.sleep(1000);
			
			singlesession.ConfirmDeleteWorkoutSession.click();
			Thread.sleep(2000);	
		}	


}
