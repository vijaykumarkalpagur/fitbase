package com.fitbaseApp.modules.actions;

import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;

import com.fitbaseApp.modules.pageObjects.Fitbase_DevicesPage;
import com.fitbaseApp.utilities.Log;

public class Fitbase_DevicesAction {

	public static void ClickOnDevicesModule(WebDriver driver){
		
		Fitbase_DevicesPage devices=new Fitbase_DevicesPage(driver);
			devices.lnk_devices.click();			
		}
	
	// Sync with Fitbit
	public static void synWithFitbit(WebDriver driver) throws Exception{
			
		Fitbase_DevicesPage fitbit=new Fitbase_DevicesPage(driver);
		
			try{
				Thread.sleep(3000);
			if(fitbit.btn_connectFitbit.isDisplayed()){
				Log.info("Click action performed on Fitbit connect button");
				fitbit.btn_connectFitbit.click();
				
				Thread.sleep(3000);
				Log.info("Enter Fitbit credentials");
				fitbit.txt_fitbitEmail.sendKeys("priyaindugula22@gmail.com");
				fitbit.txt_fitbitPassword.sendKeys("password");	
				
				Thread.sleep(3000);
				Log.info("Click action performed on Fitbit Login button");
				fitbit.btn_FitbitLogin.click();
				
				fitbit.checkbox_AllowAll.click();
				fitbit.btn_FitbitAllow.click();
				
				Thread.sleep(3000);
				Log.info("Click action performed on Fitbit sync button");
				fitbit.btn_syncFitbitData.click();	
				
				
				Thread.sleep(3000);
				Assert.assertEquals(fitbit.msg_FitbitLastSync.getText(), "Last synced 0 minutes ago");
				System.out.println(fitbit.msg_FitbitLastSync.getText());
		     }
			else {
				Log.info("Click action performed on Fitbit sync button");
		    	fitbit.btn_syncFitbitData.click();
			
			    Thread.sleep(2000);
			}		
			}
			catch(NoSuchElementException e){
				Log.info("Connect button is not displayed" + e);
			}catch(Exception e){}
	}
	
	
	
	// Sync with Misfit
	
		public static void synWithMisfit(WebDriver driver) throws Exception{
				
			Fitbase_DevicesPage misfit=new Fitbase_DevicesPage(driver);
			
				try{
					Thread.sleep(3000);
				if(misfit.btn_connectMisfit.isDisplayed()){
					Log.info("Click action performed on Misfit connect button");
					misfit.btn_connectMisfit.click();
					
					Thread.sleep(3000);
					Log.info("Enter Misfit credentials");
					misfit.txt_MisfitEmail.sendKeys("vsipl.misfit@gmail.com");
					misfit.txt_MisfitPassword.sendKeys("Veritis@123");	
					
					Thread.sleep(3000);
					Log.info("Click action performed on Misfit Login button");
					misfit.btn_MisfitLogin.click();
				
					Thread.sleep(3000);
					Log.info("Click action performed on Fitbit sync button");
					misfit.btn_syncMisfitData.click();	
					
					
					Thread.sleep(3000);
					Assert.assertEquals(misfit.msg_MisfitLastSync.getText(), "Last synced 0 minutes ago");
					System.out.println(misfit.msg_MisfitLastSync.getText());
			     }
				else {
					Log.info("Click action performed on Misfit sync button");
					misfit.btn_syncMisfitData.click();
				
				    Thread.sleep(2000);
				}		
				}
				catch(NoSuchElementException e){
					Log.info("Connect button is not displayed" + e);
				}catch(Exception e){}
		}
	

		// Sync with NOKIA
		
			public static void synWithNokia(WebDriver driver) throws Exception{
					
				Fitbase_DevicesPage nokia=new Fitbase_DevicesPage(driver);
				
					try{
						Thread.sleep(3000);
					if(nokia.btn_connectNokia.isDisplayed()){
						Log.info("Click action performed on Nokia connect button");
						nokia.btn_connectNokia.click();
						
						Thread.sleep(3000);
						Log.info("Enter Nokia credentials");
						nokia.txt_MisfitEmail.sendKeys("priyaindugula22@gmail.com");
						nokia.txt_MisfitPassword.sendKeys("priyalaya");	
						
						Thread.sleep(3000);
						Log.info("Click action performed on Nokia Login button");
						nokia.btn_NokiaLogin.click();
												
						Thread.sleep(3000);
						Log.info("Click action performed on Nokia Allow button");
						nokia.btn_NokiaAllow.click();
					
						Actions actions = new Actions(driver);
						actions.keyDown(Keys.CONTROL).sendKeys(Keys.END).sendKeys(Keys.END).sendKeys(Keys.END).sendKeys(Keys.END).perform();
						
						Thread.sleep(3000);
						Log.info("Click action performed on Nokia sync button");
						nokia.btn_syncNokiaData.click();	
						
						
						Thread.sleep(3000);
						Assert.assertEquals(nokia.msg_NokiaLastSync.getText(), "Last synced 0 minutes ago");
						System.out.println(nokia.msg_NokiaLastSync.getText());
				     }
					else {
						Log.info("Click action performed on Nokia sync button");
						nokia.btn_syncNokiaData.click();
					
					    Thread.sleep(2000);
					}		
					}
					catch(NoSuchElementException e){
						Log.info("Connect button is not displayed" + e);
					}catch(Exception e){}
			}
		
		
			// Sync with STRAVA
			
				public static void synWithStrava(WebDriver driver) throws Exception{
						
					Fitbase_DevicesPage strava=new Fitbase_DevicesPage(driver);
					
						try{
							Thread.sleep(3000);
						if(strava.btn_connectStrava.isDisplayed()){
							Log.info("Click action performed on strava connect button");
							strava.btn_connectStrava.click();
							
							Thread.sleep(3000);
							Log.info("Enter strava credentials");
							strava.txt_StravaEmail.sendKeys("priyaindugula22@gmail.com");
							strava.txt_StravaPassword.sendKeys("priyalaya");
							
							strava.btn_StravaCookie.click();
							
							Thread.sleep(3000);
							Log.info("Click action performed on strava Login button");
							strava.btn_StravaLogin.click();
													
							Thread.sleep(3000);
							Log.info("Click action performed on strava Authorize button");
							strava.btn_StravaAuthorize.click();
						
							Actions actions = new Actions(driver);
							actions.keyDown(Keys.CONTROL).sendKeys(Keys.END).sendKeys(Keys.END).sendKeys(Keys.END).sendKeys(Keys.END).perform();
							actions.keyDown(Keys.CONTROL).sendKeys(Keys.END).sendKeys(Keys.END).sendKeys(Keys.END).sendKeys(Keys.END).perform();
							Thread.sleep(3000);
							Log.info("Click action performed on strava sync button");
							strava.btn_syncStravaData.click();	
							
							
							Thread.sleep(3000);
							Assert.assertEquals(strava.msg_StravaLastSync.getText(), "Last synced 0 minutes ago");
							System.out.println(strava.msg_StravaLastSync.getText());
					     }
						else {
							Log.info("Click action performed on strava sync button");
							strava.btn_syncStravaData.click();
						
						    Thread.sleep(2000);
						}		
						}
						catch(NoSuchElementException e){
							Log.info("Connect button is not displayed" + e);
						}catch(Exception e){}
				}
				
				// Sync with Polar
				
				public static void synWithPolar(WebDriver driver) throws Exception{
						
					Fitbase_DevicesPage polar=new Fitbase_DevicesPage(driver);
					
						try{
							Thread.sleep(3000);
						if(polar.btn_connectPolar.isDisplayed()){
							Log.info("Click action performed on polar connect button");
							polar.btn_connectPolar.click();
							
							Thread.sleep(3000);
							Log.info("Enter polar credentials");
							polar.txt_PolarEmail.sendKeys("priyaindugula22@gmail.com");
							polar.txt_PolarPassword.sendKeys("priyalaya");
														
							Thread.sleep(3000);
							Log.info("Click action performed on polar Login button");
							polar.btn_PolarLogin.click();
													
							Thread.sleep(3000);
							//Log.info("Click action performed on strava Authorize button");
							//polar.btn_PolarAuthorize.click();
						
							Actions actions = new Actions(driver);
							actions.keyDown(Keys.CONTROL).sendKeys(Keys.END).sendKeys(Keys.END).sendKeys(Keys.END).sendKeys(Keys.END).perform();
							actions.keyDown(Keys.CONTROL).sendKeys(Keys.END).sendKeys(Keys.END).sendKeys(Keys.END).sendKeys(Keys.END).perform();
							Thread.sleep(3000);
							Log.info("Click action performed on polar sync button");
							polar.btn_syncPolarData.click();	
							
							
							Thread.sleep(3000);
							Assert.assertEquals(polar.msg_PolarLastSync.getText(), "Last synced 0 minutes ago");
							System.out.println(polar.msg_PolarLastSync.getText());
					     }
						else {
							Log.info("Click action performed on polar sync button");
							polar.btn_syncPolarData.click();
						
						    Thread.sleep(2000);
						}		
						}
						catch(NoSuchElementException e){
							Log.info("Connect button is not displayed" + e);
						}catch(Exception e){}
				}
			
}
