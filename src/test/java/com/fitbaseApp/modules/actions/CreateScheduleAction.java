package com.fitbaseApp.modules.actions;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;
import com.fitbaseApp.modules.pageObjects.Trainer_CreateSchedulePageObject;
import com.fitbaseApp.utilities.Log;

public class CreateScheduleAction {


public static void createCustomSchedule(WebDriver driver) throws Exception{

		
	    Trainer_CreateSchedulePageObject createSchedule = new Trainer_CreateSchedulePageObject(driver);
		
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		Thread.sleep(2000); 
		
		
		try{
			if(driver.findElement(By.xpath("//button[@type='button' and  @class='close']")).isDisplayed()){
			driver.findElement(By.xpath("//button[@type='button' and  @class='close']")).click();
			
			}
			
		}catch (Exception e){
			Log.info("No free 15 min session for single / group found");
		
		}
		
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		Thread.sleep(3000); 
		
		// Click on create schedule link from workout sessions module
		Log.info("Click on Create schedule link......"); 
		createSchedule.lnk_CreateSchedule.isDisplayed();
		createSchedule.lnk_CreateSchedule.click();
		Thread.sleep(2000); 
		
		// Enter Name of the Workout Session
		Log.info("Enter Name of the Workout Session ......");
		createSchedule.txt_NameOfTheWorkoutSession.isDisplayed();
		createSchedule.txt_NameOfTheWorkoutSession.sendKeys("Group session with 4 users");
		//Thread.sleep(2000); 
		
		// Select type of Workout Session
		Log.info("Select type of Workout Session......");
		createSchedule.dropdown_TypeOfWorkoutSession.isDisplayed();
		createSchedule.dropdown_TypeOfWorkoutSession.click();
		//Thread.sleep(2000); 
				
		
		Select TypeOfWorkout = new Select(createSchedule.dropdown_SelectOfWorkoutSession); 
		TypeOfWorkout.selectByIndex(1);
		//driver.findElement(By.xpath("//div[@class='form-group']//select/option[@value='Group Session']")).click();
		//Assert.assertTrue(driver.findElement(By.xpath("//div[@class='form-group']//select/option[@value='Group Session']")).isSelected());


		driver.findElement(By.xpath("//form[@name='createScheduleForm']//label[@for='workout']/..//div[@class='btn-group']")).isDisplayed();
		driver.findElement(By.xpath("//form[@name='createScheduleForm']//label[@for='workout']/..//div[@class='btn-group']")).click();
	
		// Workout activity dropdown 
		
		//Thread.sleep(1000);
		driver.findElement(By.xpath("//form[@name='createScheduleForm']//label[@for='workout']/..//div[@class='btn-group']//ul/li/a[contains(text(),'Bikram Yoga')]")).isDisplayed();
		driver.findElement(By.xpath("//form[@name='createScheduleForm']//label[@for='workout']/..//div[@class='btn-group']//ul/li/a[contains(text(),'Bikram Yoga')]")).click();
		//Thread.sleep(1000);
		driver.findElement(By.xpath("//form[@name='createScheduleForm']//label[@for='workout']/..//div[@class='btn-group']")).isDisplayed();
		driver.findElement(By.xpath("//form[@name='createScheduleForm']//label[@for='workout']/..//div[@class='btn-group']")).click();
		//Thread.sleep(1000);

		Select TrainingLevel  = new Select(createSchedule.dropdown_TrainingLevel); 
		TrainingLevel.selectByValue("Expert");
		createSchedule.dropdown_TypeOfWorkoutSession.click();
		
		// Click on Recurring Radio button
		Log.info("Select Recurring radio button......");
		createSchedule.radio_RecurringSession.isDisplayed();
		createSchedule.radio_RecurringSession.click();
			
		// Click on custom Radio button
		Log.info("Select custom radio button......");
		createSchedule.radio_Custom.isDisplayed();
		createSchedule.radio_Custom.click();
		//Thread.sleep(2000); 
			
		
		// Enter start date data
		Log.info("Enter start date...........");
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM"
				+ "/dd/yyyy");
		createSchedule.txt_CustomSessionStartDate1.isDisplayed();
		createSchedule.txt_CustomSessionStartDate1.sendKeys(dateFormat.format(new Date()));
		Thread.sleep(2000); 
		
		createSchedule.txt_StartsFromTime.isDisplayed();
		createSchedule.txt_StartsFromTime.click();
		createSchedule.txt_StartsFromTime.sendKeys("09:45 AM");
		
		createSchedule.txt_Duration.isDisplayed();
		Thread.sleep(3000);
		Select WorkoutDuration  = new Select(createSchedule.txt_Duration); 
	
		WorkoutDuration.selectByIndex(3);
		
		// Click on Add more button
		createSchedule.btn_AddMore.isDisplayed();
		createSchedule.btn_AddMore.click();
		
		// Second start date text-box
		createSchedule.txt_StartDate2.click();
		createSchedule.txt_StartDate2.clear();
		
		createSchedule.txt_StartDate2.sendKeys(dateFormat.format(new Date()));

		createSchedule.txt_StartsFromTime2.isDisplayed();
		createSchedule.txt_StartsFromTime2.click();
		createSchedule.txt_StartsFromTime2.clear();
		createSchedule.txt_StartsFromTime2.sendKeys("11:46 AM");
		
		createSchedule.btn_AddMore.isDisplayed();
		createSchedule.btn_AddMore.click();
		
		// Third start date text box
		createSchedule.txt_StartDate3.click();
		createSchedule.txt_StartDate3.clear();
		
		createSchedule.txt_StartDate3.sendKeys(dateFormat.format(new Date()));

		createSchedule.txt_StartsFromTime3.isDisplayed();
		createSchedule.txt_StartsFromTime3.click();
		createSchedule.txt_StartsFromTime3.clear();
		createSchedule.txt_StartsFromTime3.sendKeys("01:47 PM");
		
		createSchedule.btn_AddMore.isDisplayed();
		createSchedule.btn_AddMore.click();
		
		// Forth start date text box
		createSchedule.txt_StartDate4.click();
		createSchedule.txt_StartDate4.clear();
				
		createSchedule.txt_StartDate4.sendKeys(dateFormat.format(new Date()));

		createSchedule.txt_StartsFromTime4.isDisplayed();
		createSchedule.txt_StartsFromTime4.click();
		createSchedule.txt_StartsFromTime4.clear();
		createSchedule.txt_StartsFromTime4.sendKeys("03:48 PM");
		
		// Enter session price
		createSchedule.txt_SessionPrice.click();
		createSchedule.txt_SessionPrice.clear();
		createSchedule.txt_SessionPrice.sendKeys("0");
		
		createSchedule.btn_PayNPublish.isDisplayed();
		createSchedule.btn_PayNPublish.click();
		
		Thread.sleep(2000);
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,12500)", "");
		
		Thread.sleep(3000);
		// Clicking pay button in checkout page
		createSchedule.btn_PayAmount.isDisplayed();
		createSchedule.btn_PayAmount.click();
		
		
		}
}
