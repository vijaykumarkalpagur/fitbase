package com.fitbaseApp.modules.actions;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import com.fitbaseApp.utilities.Log;
import com.fitbaseApp.modules.pageObjects.Trainer_LogInPage;

public class TrainerLoginAction {


public static void TrainerNormalLogin(WebDriver driver) throws Exception{

		
	Trainer_LogInPage Login = new Trainer_LogInPage(driver);
		Log.info("Login from trainer static page......"); 
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		Thread.sleep(3000); 

		// Normal login
		Log.info("Click on username field and enter email id ......");
		Login.txt_UserName.isDisplayed();
		Login.txt_UserName.sendKeys("priyaindugula3@gmail.com");
		//Thread.sleep(3000); 
		
		Log.info("Click on username field and enter password ......");
		Login.txt_Password.isDisplayed();
		Login.txt_Password.sendKeys("password");
		//Thread.sleep(3000); 
		
		Log.info("Click on Login button ......");
		Login.btn_Login.isDisplayed();
		Login.btn_Login.click();
		Thread.sleep(3000); 
	
}

public static void TrainerLogout(WebDriver driver) throws Exception{
	Trainer_LogInPage Logout = new Trainer_LogInPage(driver);
	
	Thread.sleep(2000);
	WebElement element= Logout.drp_Menu;
	JavascriptExecutor js = (JavascriptExecutor)driver;
    js.executeScript("arguments[0].click();", element);
    
    Thread.sleep(2000);
    WebElement element1= Logout.lnk_Logout;
    JavascriptExecutor js1 = (JavascriptExecutor)driver;
    js1.executeScript("arguments[0].click();", element1);
}
}