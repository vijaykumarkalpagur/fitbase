package com.fitbaseApp.modules.actions;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import com.fitbaseApp.modules.pageObjects.Trainer_CreateSchedulePageObject;
import com.fitbaseApp.modules.pageObjects.Trainer_PaymentsPage;
import com.fitbaseApp.utilities.Log;

public class PaymentsAction {
	
	public static void createScheduleToMakePayment(WebDriver driver) throws Exception{
        
	// Fill all the mandatory fields in create schedule page to check payment
	Trainer_CreateSchedulePageObject createSchedule = new Trainer_CreateSchedulePageObject(driver);
	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	
	

	try{
		if(driver.findElement(By.xpath("//button[@type='button' and  @class='close']")).isDisplayed()){
		driver.findElement(By.xpath("//button[@type='button' and  @class='close']")).click();
		
		}
		
	}catch (Exception e){
		Log.info("No free 15 min session for single / group found");
	
	}
	
	Thread.sleep(2000); 
	Log.info("Click on Create schedule link......"); 
	createSchedule.lnk_CreateSchedule.isDisplayed();
	createSchedule.lnk_CreateSchedule.click();
	
	// Enter Name of the Workout Session
	Log.info("Enter Name of the Workout Session ......");
	createSchedule.txt_NameOfTheWorkoutSession.isDisplayed();
	createSchedule.txt_NameOfTheWorkoutSession.sendKeys("Free Group session");
		
	Select TypeOfWorkout = new Select(createSchedule.dropdown_SelectOfWorkoutSession); 
	TypeOfWorkout.selectByIndex(1);
	//driver.findElement(By.xpath("//div[@class='form-group']//select/option[@value='Group Session']")).click();
	//Assert.assertTrue(driver.findElement(By.xpath("//div[@class='form-group']//select/option[@value='Group Session']")).isSelected());

    Thread.sleep(3000);
	driver.findElement(By.xpath("//form[@name='createScheduleForm']//label[@for='workout']/..//div[@class='btn-group']")).isDisplayed();
	driver.findElement(By.xpath("//form[@name='createScheduleForm']//label[@for='workout']/..//div[@class='btn-group']")).click();
		
	// Workout activity dropdown 
	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	driver.findElement(By.xpath("//form[@name='createScheduleForm']//label[@for='workout']/..//div[@class='btn-group']//ul/li/a[contains(text(),'Bikram Yoga')]")).isDisplayed();
	driver.findElement(By.xpath("//form[@name='createScheduleForm']//label[@for='workout']/..//div[@class='btn-group']//ul/li/a[contains(text(),'Bikram Yoga')]")).click();
	
	driver.findElement(By.xpath("//form[@name='createScheduleForm']//label[@for='workout']/..//div[@class='btn-group']")).isDisplayed();
	driver.findElement(By.xpath("//form[@name='createScheduleForm']//label[@for='workout']/..//div[@class='btn-group']")).click();

	
	// Enter date & time in single session
	// Enter start date data
	Log.info("Enter start date...........");
	SimpleDateFormat dateFormat = new SimpleDateFormat("MM"
					+ "/dd/yyyy");
	createSchedule.txt_singleSessionOnDateField.isDisplayed();
	createSchedule.txt_singleSessionOnDateField.sendKeys(dateFormat.format(new Date()));
				
	createSchedule.txt_singleSessionStartsFromDateField.isDisplayed();
	createSchedule.txt_singleSessionStartsFromDateField.click();
	createSchedule.txt_singleSessionStartsFromDateField.sendKeys("08:01 PM");
		
			
	// Enter session price
	createSchedule.txt_SessionPrice.click();
	createSchedule.txt_SessionPrice.clear();
	createSchedule.txt_SessionPrice.sendKeys("0");

	
    Actions actions = new Actions(driver);
    actions.keyDown(Keys.CONTROL).sendKeys(Keys.END).sendKeys(Keys.END).perform();
	
	createSchedule.btn_PayNPublish.isDisplayed();
	createSchedule.btn_PayNPublish.click();
	
	
	}
	
	public static void StripePaymentWithSavedCards(WebDriver driver) throws Exception{
     
		
		    Trainer_PaymentsPage payment = new Trainer_PaymentsPage(driver);	
			// Saved cards tab
			Log.info("Click on Paynow button from saved cards tab ......");
			

			Thread.sleep(4000);
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("window.scrollBy(0,25000)", "");
			
			Thread.sleep(2000);
			/*payment.savedCardsTab.isDisplayed();
			payment.savedCardsTab.click();
						
			Thread.sleep(2000);
	        WebElement element1 = driver.findElement(By.xpath("html/body/div[1]/section/div/div/section/div/div/div[2]/div/div/div[3]/ul/li[1]/button"));
	        //in order to click a non visible element
	        JavascriptExecutor js1 = (JavascriptExecutor)driver;
	        js1.executeScript("arguments[0].click();", element1);*/
			
			payment.btn_StripePayNow.isDisplayed();
			payment.btn_StripePayNow.click();
			Thread.sleep(4000);
	} 
	 

	public static void PaymentWithcreditRDebitCards(WebDriver driver) throws Exception{

		
	    Trainer_PaymentsPage payment = new Trainer_PaymentsPage(driver);
	   		
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        Thread.sleep(3000);   
        
        Thread.sleep(2000);
        WebElement element1 = driver.findElement(By.xpath("//div[@class = 'fb-pay-opt' and @id ='paymentDisable']//ul[@class = 'nav nav-tabs']//li[2]//button"));
        //in order to click a non visible element
        JavascriptExecutor js1 = (JavascriptExecutor)driver;
        js1.executeScript("arguments[0].click();", element1);
        
        /* payment.creditNdebitcardTab.click();
    	Log.info("Click on Paynow button from credit / debit card tab ......");
		// Credit/Debit card Tab
    	*/
	    //driver.findElement(By.xpath("//label[@class='label-field']")).click();
        //Thread.sleep(3000);    
	    
	    /* driver.switchTo().frame("__privateStripeFrame8");
	    
	    driver.navigate().refresh();
	    driver.switchTo().frame("__privateStripeFrame8");*/
	    
        
        // JavascriptExecutor js2 = (JavascriptExecutor)driver;
        // js1.executeScript("document.getElementByxpath('//*[@id='root']/form/div/div[2]/span[1]/span[2]/label/input').value='4242424242424242';");
        
        /*Actions action= new Actions(driver);
        action.moveToElement(driver.findElement(By.xpath("//input[@placeholder='Card number' and @name='cardnumber']"))).build().perform();
        */
       
        
        /*WebElement element2 = driver.findElement(By.xpath("//div//div//div//span//span//label//input[@type='tel' and @autocomplete='cc-number' and @autocorrect='off' and @spellcheck='false' and @name='cardnumber']"));
        //in order to click a non visible element
        JavascriptExecutor js2 = (JavascriptExecutor)driver;
        js2.executeScript("arguments[0].click();", element2);
        */        
    	Thread.sleep(3000);
    	
    	payment.txt_CardNumber.clear();
    	
    	WebElement txt_card = driver.findElement(By.xpath("//div//div//div//span//span//label//input[@type='tel' and @autocomplete='cc-number' and @autocorrect='off' and @spellcheck='false' and @name='cardnumber']"));
    	JavascriptExecutor myExecutor = ((JavascriptExecutor) driver);
    	myExecutor.executeScript("arguments[0].value='4242424242424242';", txt_card);
    	
    	
		payment.txt_CardNumber.sendKeys("4242424242424242");
		Log.info("Send card number....");
		
		Thread.sleep(3000);
		payment.txt_MonthNYear.isDisplayed();
		payment.txt_MonthNYear.sendKeys("0555");
		Log.info("send month and year");
		
		Thread.sleep(5000);
		payment.txt_CVC.isDisplayed();
		payment.txt_CVC.sendKeys("555");
		
		Actions action= new Actions(driver);
		action.keyDown(Keys.CONTROL).sendKeys(Keys.END).sendKeys(Keys.END).perform();
		
		payment.btn_StripePayNow.isDisplayed();
		payment.btn_StripePayNow.click();
    }
	
public static void PaypalPayment(WebDriver driver) throws Exception{

		Trainer_PaymentsPage payment = new Trainer_PaymentsPage(driver);
		Thread.sleep(3000);
	    Actions actions = new Actions(driver);
	    /*actions.moveToElement(driver.findElement(By.xpath("//div[@class = 'fb-pay-opt' and @id ='paymentDisable']//ul[@class = 'nav nav-tabs']//li[3]//button"))).build().perform();
	    actions.moveToElement(driver.findElement(By.xpath("//div[@class = 'fb-pay-opt' and @id ='paymentDisable']//ul[@class = 'nav nav-tabs']//li[3]//button"))).click();
	    */
	    Thread.sleep(2000);
        WebElement element1 = driver.findElement(By.xpath("//div[@class = 'fb-pay-opt' and @id ='paymentDisable']//ul[@class = 'nav nav-tabs']//li[3]//button"));
        //in order to click a non visible element
        JavascriptExecutor js1 = (JavascriptExecutor)driver;
        js1.executeScript("arguments[0].click();", element1);
        
	   // payment.paypalTab.isDisplayed();
		//payment.paypalTab.click();
	    
		actions.keyDown(Keys.CONTROL).sendKeys(Keys.END).sendKeys(Keys.END).perform();
		
		// credit / debitcard Tab
		Log.info("Click on Paynow button from payPal tab ......");		
		Thread.sleep(3000);

		actions.moveToElement(driver.findElement(By.xpath("//button[@type= 'submit' and @class='pay']"))).build().perform();
	    actions.moveToElement(driver.findElement(By.xpath("//button[@type= 'submit' and @class='pay']"))).click();
	    
		
		payment.btn_PaypalPayNow.click();
		Thread.sleep(4000);
		// Sandbox paypal login
		payment.lnk_payPalLogin.isDisplayed();
		payment.lnk_payPalLogin.click();
		
		payment.txt_PaypalLoginEmail.isDisplayed();
		payment.txt_PaypalLoginEmail.sendKeys("priyaindugula1@gmail.com");
		
		payment.btn_PaypalNext.isDisplayed();
		payment.btn_PaypalNext.click();
		
		payment.txt_PaypalLoginPassword.isDisplayed();
		payment.txt_PaypalLoginPassword.sendKeys("priyalaya");
		
		payment.btn_PaypalLogin.isDisplayed();
		payment.btn_PaypalLogin.click();
				
	boolean staleElement = true; 
		while(staleElement){
			
			
		  try{
			  
			  Thread.sleep(6000);
			  actions.keyDown(Keys.CONTROL).sendKeys(Keys.END).sendKeys(Keys.END).sendKeys(Keys.END).sendKeys(Keys.END).perform();
			  
			    actions.moveToElement(driver.findElement(By.xpath("//input[@type='submit' and @value='Continue' and @id='confirmButtonTop' and @validate-submit='onPay()']"))).build().perform();
			    actions.moveToElement(driver.findElement(By.xpath("//input[@type='submit' and @value='Continue' and @id='confirmButtonTop' and @validate-submit='onPay()']"))).click();
				
			    payment.btn_PaypalContinuePayment.isDisplayed();
				payment.btn_PaypalContinuePayment.click();	
			  
			  staleElement = false;

		  } catch(StaleElementReferenceException e){
		    staleElement = true;
		  }
	}		
    }
	}