package com.fitbaseApp.modules.actions;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import com.fitbaseApp.modules.pageObjects.Fitbase_LoginPage;
//import com.fitbaseApp.utilities.Constant;
import com.fitbaseApp.utilities.Log;

public class Fitbase_SocialLogInAction {
	
	public static void clickOnFitbaseLoginButtonFromStaticPage(WebDriver driver) throws Exception{

		
		Fitbase_LoginPage fitbaseLogin = new Fitbase_LoginPage(driver);
		
		//Log.info("Opening Fitbase home page on browser......"); 
		//driver.get(Constant.URL);
		
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		Thread.sleep(3000); 
		
		// Click on Fitbase signup/login button 
		Log.info("Click on Signup / Login link on static home page ......");
		
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		fitbaseLogin.btnMainLogin.isDisplayed();
		fitbaseLogin.btnMainLogin.click();	
	}
	
public static void LoginWithFacebook(WebDriver driver) throws Exception{

		
	Fitbase_LoginPage facebooklogin = new Fitbase_LoginPage(driver);
		
	    Thread.sleep(3000); 
		//Click on Facebook login button
		Log.info("Click on facebook login button ......");
		facebooklogin.btn_facebook.isDisplayed();
		facebooklogin.btn_facebook.click();
		
		ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
		driver.switchTo().window((String) tabs.get(1));
		Log.info("Switched to next opened window.");
		Thread.sleep(3000);
		
		//Enter facebook login credentials
		Log.info("Enter username......");
		//trainerlogin.txt_facebookUserName.isDisplayed();
		facebooklogin.txt_facebookUserName.sendKeys("fitbase.mail@gmail.com");
		
		//Enter google login credentials
		Log.info("Enter password......");
		//trainerlogin.txt_facebookPassword.isDisplayed();
		facebooklogin.txt_facebookPassword.sendKeys("testing123$");
		
		//Click on submit button
		Log.info("Click on login button......");
		//trainerlogin.btn_facebookLogin.isDisplayed();
		facebooklogin.btn_facebookLogin.click();
		Thread.sleep(2000);
		
		driver.findElement(By.xpath("//*[@id='platformDialogForm']/div[3]/div/table/tbody/tr/td[2]/button[1]")).click();
		
		Thread.sleep(10000);
		driver.switchTo().window((String) tabs.get(0));
		Log.info("Switching back to previous window.");	
	}

public static void LoginWithGoogle(WebDriver driver) throws Exception{

	
	Fitbase_LoginPage googleLogin = new Fitbase_LoginPage(driver);
	Thread.sleep(3000);
	//Click on Google login button
	Log.info("Click on gmail login button ......");
	googleLogin.btn_gmail.isDisplayed();
	googleLogin.btn_gmail.click();
	
	ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
	driver.switchTo().window((String) tabs.get(1));
	Log.info("Switched to next opened window.");
	Thread.sleep(3000);
	
	//Enter google login credentials
	Log.info("Enter username......");
	//googleLogin.txt_gmailUserName.isDisplayed();
	googleLogin.txt_gmailUserName.sendKeys("lillyemp@gmail.com");
	Thread.sleep(2000);
	
	//Click on Next button
	Log.info("Click on Next button......");
	googleLogin.btn_gmailNext.click();
	Thread.sleep(3000);;
	//Enter google login credentials
	Log.info("Enter password......");
	//googleLogin.txt_gmailPassword.isDisplayed();
	googleLogin.txt_gmailPassword.sendKeys("9908097413");
	Thread.sleep(2000);
	//Click on submit button
	Log.info("Click on login button......");
	//googleLogin.btn_facebookLogin.isDisplayed();
	googleLogin.btn_gmailLogin.click();
	Thread.sleep(1000);
	driver.switchTo().window((String) tabs.get(0));
	Log.info("Switching back to previous window.");
		
	
}
}
