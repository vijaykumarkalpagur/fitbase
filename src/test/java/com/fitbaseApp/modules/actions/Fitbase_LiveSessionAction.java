package com.fitbaseApp.modules.actions;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.fitbaseApp.modules.pageObjects.Fitbase_LiveSessionsPageObjects;
import com.fitbaseApp.utilities.Log;

public class Fitbase_LiveSessionAction {

public static void clickOnLiveSessionsModule(WebDriver driver) throws Exception{

	Fitbase_LiveSessionsPageObjects liveSessions = new Fitbase_LiveSessionsPageObjects(driver);
	        //Click on LiveSessions module......
			Log.info("Click on LiveSessions module......"); 
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
            Thread.sleep(3000);
			liveSessions.lnk_LiveSessionModule.isDisplayed();
			liveSessions.lnk_LiveSessionModule.click();
           }

public static void sortBytypeGroup(WebDriver driver) throws Exception{

	Fitbase_LiveSessionsPageObjects sortBytype = new Fitbase_LiveSessionsPageObjects(driver);
	       
	sortBytype.value_Group.click();
	Log.info("Selected group type"); 
}

public static void selectExpertLevel(WebDriver driver) throws Exception{

	Fitbase_LiveSessionsPageObjects selectExpertLevel = new Fitbase_LiveSessionsPageObjects(driver);
	       
	selectExpertLevel.value_ExpertLevel.click();
	Log.info("Selected Expert level"); 
}


public static void selectFreePriceDropdown(WebDriver driver) throws Exception{

	Fitbase_LiveSessionsPageObjects book = new Fitbase_LiveSessionsPageObjects(driver);
	       
			book.value_FreeSessions.click();
			Log.info("Clicked on Price dropdown & Selected free sessions......"); 
}

public static void selectPaidPriceDropdown(WebDriver driver) throws Exception{

	Fitbase_LiveSessionsPageObjects book = new Fitbase_LiveSessionsPageObjects(driver);
	       
			book.value_PaidSessions.click();
			Log.info("Clicked on Price dropdown & Selected paid sessions......"); 
}

public static void bookALiveSessions(WebDriver driver) throws Exception{

	Fitbase_LiveSessionsPageObjects book = new Fitbase_LiveSessionsPageObjects(driver);
	        //Book a LiveSession......
			Log.info("Book a LiveSession......"); 
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
						
			Thread.sleep(2000);
		    WebElement element1 = driver.findElement(By.xpath("//*[@id='schedulewkt-list-wrapper']/div[1]/a/div[2]/div[2]/h4"));
		    //in order to click a non visible element
		        JavascriptExecutor js1 = (JavascriptExecutor)driver;
		        js1.executeScript("arguments[0].click();", element1);
			
			Thread.sleep(4000);
			// Page down
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("window.scrollBy(0,250)", "");
			
			//Click on enroll button
			Log.info("Click on enroll now button.....");
			book.btn_EnrollNow.isDisplayed();
			book.btn_EnrollNow.click();
			
			try{
			Thread.sleep(3000);	
			book.btn_ConflictPopUp.isDisplayed();
			book.btn_ConflictPopUp.click();
			
			}
			catch(Exception e){	
			Log.info("No conflict pop-up found");	
			}
			
			book.btn_EnrollNowbtnInPopUp.isDisplayed();
			book.btn_EnrollNowbtnInPopUp.click();
			Thread.sleep(3000);
			
           }


public static void payWithSavedCard(WebDriver driver) throws Exception{

	Fitbase_LiveSessionsPageObjects paywithStripe = new Fitbase_LiveSessionsPageObjects(driver);
	        //Book a LiveSession......
			Log.info("Book a LiveSession......"); 
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

			//Click action performed on Saved cards tab......
			Log.info("Click on a SavedCards tab......"); 
			paywithStripe.tab_SavedCards.isDisplayed();
			paywithStripe.tab_SavedCards.click();
			
			//Click on pay button
			Log.info("Click on stripe enroll now button.....");
			paywithStripe.btn_StripePayNow.isDisplayed();
			paywithStripe.btn_StripePayNow.click();
			Thread.sleep(3000);
			
           }

public static void payWithCreditOrCreditCard(WebDriver driver) throws Exception{

	Fitbase_LiveSessionsPageObjects paywithStripe = new Fitbase_LiveSessionsPageObjects(driver);
	        //Book a LiveSession......
			Log.info("Book a LiveSession......"); 
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

			//Click action performed on Saved cards tab......
			Log.info("Click on a Credit Or CreditCard tab......"); 
			paywithStripe.tab_CreditORdebitCard.isDisplayed();
			paywithStripe.tab_CreditORdebitCard.click();
			
			paywithStripe.EnterCardNumber.sendKeys("42424242424242424242424");
			Log.info("Enter Valid Credit/DebitCard Number");
     		Thread.sleep(3000);
     		
     		paywithStripe.ClickonPayButton.click();
     		Thread.sleep(15000);
    		Log.info("Clicked on PayNow Button");
			
           }


public static void payWithPaypal(WebDriver driver) throws Exception{

	Fitbase_LiveSessionsPageObjects paywithPaypal = new Fitbase_LiveSessionsPageObjects(driver);
	        //Book a LiveSession......
			Log.info("Book a LiveSession......"); 
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

			Actions actions = new Actions(driver);
		    
		    Thread.sleep(2000);
	        WebElement element1 = driver.findElement(By.xpath("//div[@class = 'fb-pay-opt' and @id ='paymentDisable']//ul[@class = 'nav nav-tabs']//li[3]//a"));
	        //in order to click a non visible element
	        JavascriptExecutor js1 = (JavascriptExecutor)driver;
	        js1.executeScript("arguments[0].click();", element1);
	        
		 
			actions.keyDown(Keys.CONTROL).sendKeys(Keys.END).sendKeys(Keys.END).perform();
			
			// credit / debitcard Tab
			Log.info("Click on Paynow button from payPal tab ......");		
			Thread.sleep(3000);
   
			
		    paywithPaypal.btn_PaypalPayNow.click();
			Thread.sleep(4000);
			// Sandbox paypal login
			paywithPaypal.lnk_payPalLogin.isDisplayed();
			paywithPaypal.lnk_payPalLogin.click();
			Thread.sleep(2500);

//			paywithPaypal.txt_PaypalLoginEmail.sendKeys("priyaindugula1@gmail.com");
			driver.findElement(By.xpath("//*[@id='email']")).sendKeys("bsbsfgdbfdb");;
			
			paywithPaypal.btn_PaypalNext.click();
			
			Thread.sleep(1500);
			paywithPaypal.txt_PaypalLoginPassword.isDisplayed();
			paywithPaypal.txt_PaypalLoginPassword.sendKeys("priyalaya");
			
			paywithPaypal.btn_PaypalLogin.isDisplayed();
			paywithPaypal.btn_PaypalLogin.click();
					
		boolean staleElement = true; 
			while(staleElement){
				
				
			  try{
				  
				  Thread.sleep(6000);
				  actions.keyDown(Keys.CONTROL).sendKeys(Keys.END).sendKeys(Keys.END).sendKeys(Keys.END).sendKeys(Keys.END).perform();
				  
				    actions.moveToElement(driver.findElement(By.xpath("//input[@type='submit' and @value='Continue' and @id='confirmButtonTop' and @validate-submit='onPay()']"))).build().perform();
				    actions.moveToElement(driver.findElement(By.xpath("//input[@type='submit' and @value='Continue' and @id='confirmButtonTop' and @validate-submit='onPay()']"))).click();
					
				    paywithPaypal.btn_PaypalContinuePayment.isDisplayed();
				    paywithPaypal.btn_PaypalContinuePayment.click();	
				  
				  staleElement = false;

			  } catch(StaleElementReferenceException e){
			    staleElement = true;
			  }
			}
}		


public static void PaymentWithPaypal(WebDriver driver) throws Exception{
	Fitbase_LiveSessionsPageObjects Payment1withPaypal = new Fitbase_LiveSessionsPageObjects(driver);

//Payment1withPaypal.ClickonPayPalTab.click();
//Log.info("Click on PayPal Tab");

driver.findElement(By.xpath("//*[@id='paymentDisable']/ul/li[3]/a")).click();
Thread.sleep(1500);

driver.findElement(By.xpath("//*[@id='paypal']/div/button")).click();
//Payment1withPaypal.ContinuetoPayPalButton.click();
Thread.sleep(5000);

driver.findElement(By.xpath("//*[@id='loginSection']/div/div[2]/a")).click();
Payment1withPaypal.PayPalLoginButton.click();
Thread.sleep(5000);

driver.findElement(By.xpath("//*[@id='email']")).sendKeys("vijaykumarkalpagur@gmail.com");

//Payment1withPaypal.EnterPayPalEmailID.sendKeys("vijaykumarkalpagur@gmail.com");
Payment1withPaypal.ClickOnNextButton.click();
Thread.sleep(2000);

Payment1withPaypal.EnterPayPalPassword.sendKeys("Testing@123");
Payment1withPaypal.ClickOnLoginButton.click();
Thread.sleep(4000);

driver.findElement(By.xpath("//*[@id='confirmButtonTop']")).click();

}
}