package com.fitbaseApp.modules.actions;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;

//import com.fitbaseApp.utilities.Constant;
import com.fitbaseApp.utilities.Log;
import com.fitbaseApp.modules.pageObjects.Trainer_LogInPage;


public class TrainerSocialLoginAction {

	public static void clickOnTrainerLoginButtonFromStaticPage(WebDriver driver) throws Exception{

		
		Trainer_LogInPage trainerlogin = new Trainer_LogInPage(driver);

		
		Log.info("Opening Fitbase home page on browser......"); 
		/*driver.get(Constant.URL);*/
		
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		Thread.sleep(3000); 

		// Click on Trainer sign up link from static page
		Log.info("Click on Signup / Login link on static page ......");
		trainerlogin.btnTrainerSignUpInStaticPage.isDisplayed();
		trainerlogin.btnTrainerSignUpInStaticPage.click();
		
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		Thread.sleep(3000); 
				
		ArrayList<String> trainerTab = new ArrayList<String> (driver.getWindowHandles());
	    driver.switchTo().window(trainerTab.get(1));
	   // driver.close();
	   // driver.switchTo().window(tabs2.get(0));
		
		// Click on Trainer signup/login button from trainer static page
		Log.info("Click on Signup / Login link on trainer home page ......");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		Thread.sleep(3000); 
		trainerlogin.mainSignUpLoginBtn.isDisplayed();
		trainerlogin.mainSignUpLoginBtn.click();
		
	}
	
public static void LoginWithFacebook(WebDriver driver) throws Exception{

		
		Trainer_LogInPage facebooklogin = new Trainer_LogInPage(driver);
		
		//Click on Facebook login button
		Log.info("Click on facebook login button ......");
		facebooklogin.btn_facebook.isDisplayed();
		facebooklogin.btn_facebook.click();
		
		ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
		driver.switchTo().window((String) tabs.get(2));
		Log.info("Switched to next opened window.");
		Thread.sleep(3000);
		//Enter facebook login credentials
		Log.info("Enter username......");
		//trainerlogin.txt_facebookUserName.isDisplayed();
		facebooklogin.txt_facebookUserName.sendKeys("fitbase.mail@gmail.com");
		
		//Enter google login credentials
		Log.info("Enter password......");
		//trainerlogin.txt_facebookPassword.isDisplayed();
		facebooklogin.txt_facebookPassword.sendKeys("testing123$");
		
		//Click on submit button
		Log.info("Click on login button......");
		//trainerlogin.btn_facebookLogin.isDisplayed();
		facebooklogin.btn_facebookLogin.click();
		
		driver.switchTo().window((String) tabs.get(1));
		Log.info("Switching back to previous window.");
		
	}

public static void LoginWithGoogle(WebDriver driver) throws Exception{

	
	Trainer_LogInPage googleLogin = new Trainer_LogInPage(driver);
	Thread.sleep(3000);
	//Click on Google login button
	Log.info("Click on gmail login button ......");
	googleLogin.btn_gmail.isDisplayed();
	googleLogin.btn_gmail.click();
	
	ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
	driver.switchTo().window((String) tabs.get(2));
	Log.info("Switched to next opened window.");
	Thread.sleep(3000);
	
	//Enter google login credentials
	Log.info("Enter username......");
	//googleLogin.txt_gmailUserName.isDisplayed();
	googleLogin.txt_gmailUserName.sendKeys("priyaindugula3@gmail.com");
	
	//Click on Next button
	Log.info("Click on Next button......");
	googleLogin.btn_gmailNext.click();
	Thread.sleep(2000);
	//Enter google login credentials
	Log.info("Enter password......");
	//googleLogin.txt_gmailPassword.isDisplayed();
	googleLogin.txt_gmailPassword.sendKeys("priyalaya");
	
	//Click on submit button
	Log.info("Click on login button......");
	//googleLogin.btn_facebookLogin.isDisplayed();
	googleLogin.btn_gmailLogin.click();
	
	driver.switchTo().window((String) tabs.get(1));
	Log.info("Switching back to previous window.");
	
	Thread.sleep(3000);
	
}
	
	
}
