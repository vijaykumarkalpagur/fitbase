package com.fitbaseApp.modules.actions;

import java.util.concurrent.TimeUnit;

//import org.junit.Assert;
import org.openqa.selenium.WebDriver;


import com.fitbaseApp.modules.pageObjects.Trainer_VideoPageObjects;
import com.fitbaseApp.utilities.Log;

public class TrainerVideoAction {
	
public static void checkVideoFromTrainerDashboard(WebDriver driver) throws Exception{

		
	Trainer_VideoPageObjects video = new Trainer_VideoPageObjects(driver);
		
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		Thread.sleep(3000); 

		try{
		
		
		// if video is available click on Live sessions button in dash-board Or else show a help message saying no live sessions available
		
		Log.info("Click on Live sessions button in dashboard......"); 
		video.btn_LiveSession.isDisplayed();
		video.btn_LiveSession.click();
		Thread.sleep(3000); 

		/*video.msg_WaitingForUser.isDisplayed();
		Object Actualtext = video.msg_WaitingForUser.getText();
		Assert.assertEquals(Actualtext, "Waiting for user to join  ");*/
		
		}
		
		catch(Exception e){
	
			Log.info("No Live sessions is available......"); 
			System.out.print(e.getMessage());
		
		}
}
}

