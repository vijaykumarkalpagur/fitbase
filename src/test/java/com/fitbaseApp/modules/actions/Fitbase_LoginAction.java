package com.fitbaseApp.modules.actions;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import com.fitbaseApp.modules.pageObjects.Fitbase_LoginPage;
import com.fitbaseApp.utilities.Log;

public class Fitbase_LoginAction {
	public static void FitbaseNormalLogin(WebDriver driver) throws Exception{

		
		Fitbase_LoginPage Login = new Fitbase_LoginPage(driver);
			Log.info("Login from fitbase static page......"); 
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			Thread.sleep(3000); 

			// Click on Login button
			Log.info("Click on Signup/Login button in static page");
			Login.btnMainLogin.isDisplayed();
            Login.btnMainLogin.click();

			// Normal login
			Log.info("Click on username field and enter email id ......");
			Login.txt_username.isDisplayed();
			Login.txt_username.sendKeys("priyaindugula02@gmail.com");
			//Thread.sleep(3000); 
			
			Log.info("Click on username field and enter password ......");
			Login.txt_password.isDisplayed();
			Login.txt_password.sendKeys("priya123");
			//Thread.sleep(3000); 
			
			Log.info("Click on Login button ......");
			Login.btnLoginPopUp.isDisplayed();
			Login.btnLoginPopUp.click();
			Thread.sleep(3000); 
		
	}
	
	
	public static void FitbaseUser_logOut(WebDriver driver) throws Exception{
		
	Fitbase_LoginPage Logout = new Fitbase_LoginPage(driver);
	 
	Thread.sleep(2000);
	Logout.drp_menu.click();
	Logout.lnk_Logout.click();
    }
	
   public static void FitbaseMultipleUsersLogin(WebDriver driver) throws Exception{

    // Multiple user's login
	String multiUser[][]= { {"inlpriya@mailinator.com","priyaindugula02@gmail.com","priyaindugula01@gmail.com","laya.indugula@gmail.com"} , {"password","priya123","priya123","password"} };

	for(int i=0; i<4 ; i++){
		{
			int x=1;
			int z=0;
	
		 Fitbase_LoginPage Login = new Fitbase_LoginPage(driver);
		 Log.info("Login from fitbase static page......"); 
		 driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		 Thread.sleep(3000); 

		 // Click on Login button
		 Log.info("Click on Signup/Login button in static page");
		 Login.btnMainLogin.isDisplayed();
         Login.btnMainLogin.click();
         
      
      	Log.info("Click on username field and enter email id ......");
      	Login.txt_username.isDisplayed();
      	Login.txt_username.sendKeys(multiUser[z][i]);
		System.out.println("Logged-in with "+multiUser[z][i]);
      	//Thread.sleep(3000); 
      		
      	Log.info("Click on username field and enter password ......");
      	Login.txt_password.isDisplayed();
      	Login.txt_password.sendKeys(multiUser[x][i]);
      	//Thread.sleep(3000); 
      			
      	Log.info("Click on Login button ......");
      	Login.btnLoginPopUp.isDisplayed();
      	Login.btnLoginPopUp.click();
      	Thread.sleep(3000); 
      	
      	Fitbase_LiveSessionAction.clickOnLiveSessionsModule(driver);
      	Fitbase_LiveSessionAction.sortBytypeGroup(driver);
      	Thread.sleep(1500);
      	Fitbase_LiveSessionAction.selectExpertLevel(driver);
      	Thread.sleep(1500);
      	Fitbase_LiveSessionAction.selectFreePriceDropdown(driver);
      	Thread.sleep(1500);
    	Fitbase_LiveSessionAction.bookALiveSessions(driver);	
    	
    	JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,-250)", "");
    	
    	Thread.sleep(2000);
    	Login.drp_menu.click();
    	Thread.sleep(2000);
    	Login.lnk_Logout.click();
         
   }
  }
 }
}
