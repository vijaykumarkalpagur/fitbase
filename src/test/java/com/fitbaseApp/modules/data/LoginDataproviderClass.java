//Authored Krishna Yadav Dasari

package com.fitbaseApp.modules.data;

import org.testng.annotations.DataProvider;

public class LoginDataproviderClass {

	@DataProvider(name="newLoginUser")  // New  Login user
	public static Object[][] getDataFromNewLoginUserDataprovider(){
		return new Object[][] {
			{ "AILEEN","VSIPL","aileen","password"},};}

	//#############################################

	@DataProvider(name="newUser")  // New user
	public static Object[][] getDataFromNewUserDataprovider(){
		return new Object[][] {
			{ "ALBA","VSIPL","alba","password"},};}

	//#############################################

	@DataProvider(name="User")  // New user
	public static Object[][] getDataFromUserDataprovider(){
		return new Object[][] {
			{ "BEN","VSIPL","ben","password"},};}


	//#############################################

	@DataProvider(name="healthDataUser")  // New user
	public static Object[][] getDataFromHealthDataUserDataprovider(){
		return new Object[][] {
			{ "BEN","VSIPL","ben","password","5","3","136"},};}

	//#############################################

	@DataProvider(name="heavyWeightDataUser")  // New user
	public static Object[][] getDataFromHeavyWeightDataUserDataprovider(){
		return new Object[][] {
			{ "BEN","VSIPL","ben","password","5","5","555"},};}

	//#############################################

	@DataProvider(name="maintainWeightDataUser")  // New user
	public static Object[][] getDataFromMaintainWeightDataUserDataprovider(){
		return new Object[][] {
			{ "BEN","VSIPL","ben","password","5","5","125"},};}

	//#############################################

	@DataProvider(name="lowWeightDataUser")  // New user
	public static Object[][] getDataFromLowWeightDataUserDataprovider(){
		return new Object[][] {
			{ "BEN","VSIPL","ben","password","5","5","55"},};}

	//#############################################


	@DataProvider(name="NegativeHealthDataUser")
	public static Object[][] getDataFromNegativeHealthDataUserDataprovider(){
		return new Object[][] {
			{ "BEN","VSIPL","ben","password",".",".","0","5","3","136"},};}

	//#############################################

	@DataProvider(name="NegativeNewUser")  // Negative New user
	public static Object[][] getDataFromNegativeNewUserDataprovider(){
		return new Object[][] {
			{ "CHEVI","VSIPL","chevi","password"},};}

	//#############################################

	@DataProvider(name="NegativeUser")  // Negative New user
	public static Object[][] getDataFromNegativeUserDataprovider(){
		return new Object[][] {
			{ "CAROL","VSIPL","carol","password"},};}

	//#############################################

	@DataProvider(name="GmailUser")  // Gmail user
	public static Object[][] getDataFromGmailUserDataprovider(){
		return new Object[][] {
			{ "GFITBASE","VSIPL","tfitbase@gmail.com","password","testing123$"},};}

	//Gmail users:
	// tfitbase@gmail.com /testing123$
	// "vsit.pwv@gmail.com","testing123$" 
	// devteam.testteam@gmail.com / priyalaya  for gmail
	// baba4g.fitbase@gmail.com /testing123$
	// baba3g.fitbase@gmail.com

	//#############################################

	//Facebook and gmail user for acceptance testing:::::::::::
	@DataProvider(name="facebookGmailFitbaseUser")  
	public static Object[][] getDataFromFGDataprovider(){                
		return new Object[][] {
			{ "fitbase.mail@gmail.com","testing123$",
				"fitbase.mail@gmail.com","testing123$",
				"sk@gmail.com","password"},};}  

	//clwgays_fergieman_1429816511@tfbnw.net  --->Facebook user: verified user
	//fitbase.mail@gmail.com ---->  gmail user: verified user
	//sk@gmail.com ---->  fitbase user: unverified user
	//#############################################

	//Facebook user for acceptance testing::::::::::: without fill health details
	@DataProvider(name="facebookUser")  
	public static Object[][] getDataFromFacebookDataprovider(){                
		return new Object[][] {
			{ "fitbase.mail@gmail.com","testing123$",},};} 

	//"dtertve_moiduwitz_1429816416@tfbnw.net","test",

	//#############################################

	//Gmail user for acceptance testing:::::::::::
	@DataProvider(name="GUser")  
	public static Object[][] getDataFromGDataprovider(){                
		return new Object[][] {
			{ "fitbase.mail@gmail.com","testing123$",},};}  

	//fitbase.mail@gmail.com ---->  gmail user: verified user
	//#############################################

	//Gmail user for acceptance testing:::::::::::
	@DataProvider(name="GmailForgotUser")  
	public static Object[][] getDataFromGForgotDataprovider(){                
		return new Object[][] {
			{ "fitbase.mail@gmail.com","testing123$",},};}  

	//user id: fitbase.mail@gmail.com ---->  gmail user: testing for forgot user only
	//Passwor: for Gmail--> testing123$
	//password: for fitbase-->password

	//#############################################

	//Gmail user for Verify your email address:::::::::::
	@DataProvider(name="FitbaseVerifyUser")  
	public static Object[][] getDataFromFVUDataprovider(){                
		return new Object[][] {
			{ "VERITIS","Tester","vsipl.pwv@gmail.com","testing123$",},};}  //tfitbase@gmail.com  krishna111.fitbase@gmail.com/testing123$  , dkrishna.fitbase49@gmail.com

	//vsit.pwv@gmail.com ---->  gmail user: verified user  Same as gmail & Fitbase common user  //vsipl.pwv@gmail.com /testing123$ --> Phone: 8297676757

	//#############################################

	//Gmail user for Verify your email address:::::::::::
	@DataProvider(name="FitbaseVerifyUserTwo")  
	public static Object[][] getDataFromFVUDataproviderTwo(){                
		return new Object[][] {
			{ "VERITIS","Tester","vsipl2.pwv@gmail.com","testing123$",},};}      //  "vsipl2.pwv@gmail.com","testing123$":>>> Phone: 917386378488

	//#############################################

	//Gmail user for Verify your email address:::::::::::
	@DataProvider(name="FitbaseVerifyUser1")  
	public static Object[][] getDataFromFVU1Dataprovider(){                
		return new Object[][] {
			{ "VERITIS","Groups","krishna111.fitbase@gmail.com","testing123$",},};} 

	//vsit.pwv@gmail.com ---->  gmail user: verified user  Same as gmail & Fitbase common user
	//#############################################

	//Email preferences user:::::::::::::::::::::::::::
	@DataProvider(name="FitbaseInviteUser")  
	public static Object[][] getDataFromFIUDataprovider(){                
		return new Object[][] {
			{ "TRUMP","VSIPL","trump","dkrishna.fitbase49@gmail.com","testing123$",},};} 

	//dkrishna.fitbase49@gmail.com  / testing123$  for Gmail and fitbase
	//#############################################

}