package com.fitbaseApp.utilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import java.util.concurrent.TimeUnit;
import com.fitbaseApp.utilities.Constant;

public class OpenBrowser {
		public static WebDriver driver = null;
		
		public static WebDriver getDriver() {
			return driver;
		}
		
		public static WebDriver openBrowser(int iTestCaseRow) throws Exception{
			String sBrowserName;
			try{
				sBrowserName = SingleSignIn.getCellData(iTestCaseRow, Constant.Col_Browser);
			
				if (sBrowserName.equalsIgnoreCase("firefox")){
						System.setProperty("webdriver.gecko.driver", ".\\geckodriver.exe");
		        		driver = new FirefoxDriver();
		        		Log.info("Driver instantiated");
		        		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		        		driver.get(Constant.URL);
		        		driver.manage().window().maximize();
		        		Log.info("Web application launched successfully.");
				}
		    
				else if(sBrowserName.equalsIgnoreCase("chrome")){
						System.setProperty("webdriver.chrome.driver", ".\\chromedriver.exe");
						driver = new ChromeDriver();
						Log.info("Driver instantiated");
						driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
						driver.get(Constant.URL);
						driver.manage().window().maximize();
						Log.info("Web application launched successfully.");
				}
				else if(sBrowserName.equalsIgnoreCase("ie")){
		        		System.setProperty("webdriver.ie.driver",".\\IEDriverServer.exe");
		        		driver = new InternetExplorerDriver();
		        		Log.info("Driver instantiated");
		        		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		        		driver.get(Constant.URL);
		        		driver.manage().window().maximize();
		        		Log.info("Web application launched successfully.");
				}
			}
			catch (Exception e){
				Log.error("class Utils | Method OpenBrowser | Exception desc : " + e.getMessage());
			}
			return driver;
		}
		
		public static String getTestCaseName(String sTestCase) throws Exception{
			String value = sTestCase;
			try{
				int posi = value.indexOf("@");
				value  = value.substring(0, posi);
				posi = value.lastIndexOf(".");
				
				value = value.substring(posi + 1);
				return value;
			}catch (Exception e){
				Log.error("Class Utils | Method getTestCaseName | Exception desc : " + e.getMessage());
				throw(e);
			}
		}
	}

