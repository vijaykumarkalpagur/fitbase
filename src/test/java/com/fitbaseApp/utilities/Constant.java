package com.fitbaseApp.utilities;

import java.math.BigInteger;
import java.security.SecureRandom;

public class Constant{
	
	//#####################################################################
	//Select the browser from here to execute the build/script.
		
		public static final String browserName = "chrome";
	//	public static final String browserName = "firefox";
	//	public static final String browserName = "ie";
		
	//#####################################################################
		
	//Profile image file source for upload image
	public static final String prifileimage = "C:\\Users\\Public\\Pictures\\Sample Pictures\\Chrysanthemum.jpg";
	public static final String URL = "http://fitbase-acp-1089568855.us-west-2.elb.amazonaws.com";
	//public static final String URL = "https://www.fitbase.com";
	public static final String TrainerURL = "http://fitbase-acp-1089568855.us-west-2.elb.amazonaws.com/join-as-trainer.html";
	
	public static final String facebookURL = "https://www.facebook.com/";
	
	//Data Driven multiple sign ups
	public static final String multipleSignUps = System.getProperty("user.dir") + "/src/test/java/dataDrivenFiles/multipleSignUps.xlsx";
	
	//Activate mail by logging into gmail account
	public static final String gmailURL = "https://accounts.google.com/ServiceLogin?service=mail&continue=https://mail.google.com/mail/#identifier";
	//public static final String gmailUserEmail = "raparthisivasankar4@gmail.com";
	public static final String gmailPassword = "RSatish12";
	
	//Data Driven multiple sign ins
	public static final String multipleSignIns= System.getProperty("user.dir") + "/src/test/java/dataDrivenFiles/multipleSignIns.xlsx";
	
	//Method to generate random increment number
	public static int generateIncrementalNumber() {return new BigInteger(10, new SecureRandom()).intValue();}		
	
	//Data Driven single login
	public static final String Path_TestData = System.getProperty("user.dir") + "/src/test/java/dataDrivenFiles/";
	public static final String File_TestData = "TestData.xlsx"; 
	
	//Test Data Sheet Columns for single login
	public static final int Col_TestCaseName = 0;
	public static final int Col_UserName = 1;
	public static final int Col_Password = 2;
	public static final int Col_Browser = 3;
	
	//Create Group dynamic
	public static String generateRandomString() {return "MAR10" + new BigInteger(10, new SecureRandom()).intValue();}
	
	//Create, search Group
	public static String newGroupName = "CreateTheGroupAgain";
	
	//Search in new group request received
	public static String newGroupRequestReceived = "Three";
	
	//Search the non member group
	public static String nonMemberGroup = "JoinTheGroupAgain";
	
	//Search group to view
	public static String searchGroupForLeaderBoard = "DelhimorningCycling";
	
	//Search for fitbase member in invite 
	public static String searchFitbaseMember = "ght";	
	
	//Remove Member in a Group
	public static final int i = 1;		
		
	//Search Member in a Group Members
	public static final String searchMember = "Frank Mullar";
		
	//Search Member in Group Request received
	public static final String receivedName = "Ten";
		
	//Search Member in Group Request Sent
	public static final String sentName = "Chintu";
		
	//Create Challenge dynamic
	public static String generateRandomChallengeString() {return "MarchChall" + new BigInteger(10, new SecureRandom()).intValue();}
	
	//Create challenge static
	public static String newChallengeName = "JeyporeJoggers";
	public static final int currentDate = 0;
	public static final int futureDate = 365;
			
	//Search a group in join group	
	public static final String searchGroup = "JeyporeJoggers";
		
	//Invite fitbase member from a group
	public static final int inviteMembers = 20;
	
	//Search friend in Add Friends
	public static final String searchFriend = "raparthisiva";	
	
	//Edit Health Details
	public static final String weight = "2";
	public static final String heightFeet = "2";
	public static final String heightinches = "2";
	public static final String dateOfBirthMonth = "Febuary";
	public static final String dateOfBirthDate = "2";
	public static final String dateOfBirthYear = "2012";
	public static final String gender = "Male";
		
	//Food
	public static final String allFoodSearch = "Tilapia";
	public static final String myFoodSearch = "Burger";
	public static final String restaurantSearch = "alonti";
	public static final String foodName = "Indian Bread1";
	public static final int pastDate = -10;
	public static final int onFutureDate = 10;	
	
	//Workouts
	public static final int scheduleForFutureDate = 1;
}
