package com.fitbaseApp.testCases.Fitbase_Checklist;

import org.apache.log4j.xml.DOMConfigurator;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.fitbaseApp.modules.actions.Fitbase_LiveSessionAction;
import com.fitbaseApp.modules.actions.Fitbase_LoginAction;
import com.fitbaseApp.modules.helpers.BrowserHelper;
import com.fitbaseApp.utilities.Constant;
import com.fitbaseApp.utilities.Log;
import com.fitbaseApp.utilities.OpenBrowser;

public class Fitbase_EnrollLiveSessionWithPayPal {
	
	public WebDriver driver;
	private String sTestCaseName;
	private String browserName;

	@BeforeClass()

	public void setup() throws Exception {
		DOMConfigurator.configure("log4j.xml");
		sTestCaseName = this.toString();
		sTestCaseName = OpenBrowser.getTestCaseName(this.toString());
		Log.startTestCase(sTestCaseName);

		browserName = Constant.browserName;
		driver = BrowserHelper.openBrowser(browserName);
	}

	@Test(priority = 0, enabled = true)
	public void bookingALiveSessionWithPaypal() throws Exception {
		
		Fitbase_LoginAction.FitbaseNormalLogin(driver);
		Fitbase_LiveSessionAction.clickOnLiveSessionsModule(driver);
		Fitbase_LiveSessionAction.selectPaidPriceDropdown(driver);
		Fitbase_LiveSessionAction.bookALiveSessions(driver);
		Fitbase_LiveSessionAction.PaymentWithPaypal(driver);
		
	    Log.info("Test case sync with booking a live session is executed successfully.");		
	}

	@AfterClass()
	public void tearDown() throws Exception {
    Log.endTestCase(sTestCaseName);
	driver.quit();
	}
    }
