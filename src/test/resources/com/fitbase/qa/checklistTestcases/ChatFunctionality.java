package com.fitbase.qa.checklistTestcases;

import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.fitbase.qa.helper.Browserhelper;
import com.fitbase.qa.helper.ChatHelper;
import com.fitbase.qa.utilities.Log;
public class ChatFunctionality {

	static WebDriver driver;
	
	@BeforeClass
	public void openBrowser() throws Exception {
		
		Log.startLog("Test is starting");
      driver=Browserhelper.openBrowser();		
	}
	
	@Test()
	public void chatFunctionality() throws Exception {
		
        ChatHelper.chatFunctionality();
		Reporter.log("Chat functionality testcase has executed sucessfully....");
	}
	
	@AfterClass
	public void quitBrowser() {
		Log.endLog("Test is ending");
		driver.quit();
	}


}
