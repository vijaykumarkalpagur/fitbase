package com.fitbase.qa.checklistTestcases;

import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.fitbase.qa.actions.LiveConnectVideoAction;
import com.fitbase.qa.actions.LoginAction;
import com.fitbase.qa.actions.SignOutAction;
import com.fitbase.qa.helper.Browserhelper;
import com.fitbase.qa.testdata.LoginDataProvider;
import com.fitbase.qa.utilities.Log;
public class CheckLiveConnectVideo {

	static WebDriver driver;
	
	@BeforeClass
	public void openBrowser() throws Exception {
		
		Log.startLog("Test is starting");
        driver=Browserhelper.openBrowser();		
	}
	
	@Test(dataProvider="UserLogin", dataProviderClass = LoginDataProvider.class)
	public void videoFunctionality(String uname, String pswd) throws Exception {
		
		LoginAction.fitbaseNormalLogin(driver, uname, pswd);
		LiveConnectVideoAction.checkLiveConnectVideo(driver);
		//SignOutAction.signout(driver);
		Reporter.log("Live video session testcase has executed sucessfully....");}
		
	@AfterClass
	public void quitBrowser() {
		Log.endLog("Test is ending");
		//driver.quit();
	}
}
