package com.fitbase.qa.checklistTestcases;

import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.fitbase.qa.actions.LoginAction;
import com.fitbase.qa.actions.SignOutAction;
import com.fitbase.qa.helper.Browserhelper;
import com.fitbase.qa.testdata.LoginDataProvider;
import com.fitbase.qa.utilities.Log;



public class GoogleLogin{

	static WebDriver driver;
	
	@BeforeClass
	public void openBrowser() throws Exception {
		Log.startLog("Test is starting");
		driver = Browserhelper.openBrowser();
	}
	
	@Test(dataProvider="GoogleLogin", dataProviderClass=LoginDataProvider.class)
	public void googleLoginFunctionality(String gmailUserName, String gmailPassword) throws Exception{
		LoginAction.fitbaseGoogleLogin(driver, gmailUserName, gmailPassword);
		//SignOutAction.signout(driver);  
		Reporter.log("Login with Google testcase has been executed successfully...");
	}
	
	@AfterClass
	public void quitBrowser(){
		Log.endLog("Test is ending");
		driver.quit();
	}
}
