package com.fitbase.qa.checklistTestcases;

import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.fitbase.qa.actions.LiveSessionsAction;
import com.fitbase.qa.actions.LoginAction;
import com.fitbase.qa.actions.SignOutAction;
import com.fitbase.qa.helper.Browserhelper;
import com.fitbase.qa.testdata.LoginDataProvider;
import com.fitbase.qa.utilities.Log;



public class EnrollALiveSessionWithPaypal {
	static WebDriver driver;
	
	@BeforeClass
	public void openbrowser() throws Exception{
		Log.startLog("Test is starting");
		driver = Browserhelper.openBrowser();
	}

	@Test(dataProvider="SessionbookpaypalUserLogin", dataProviderClass = LoginDataProvider.class)
	public void bookALiveSessionWithPaypalFunctionality(String uname, String pswd) throws Exception {
		
		LoginAction.fitbaseNormalLogin(driver, uname, pswd);
		LiveSessionsAction.enrollApadiSessionWithPayPal(driver);
		//SignOutAction.signout(driver);
		Reporter.log("Booking a live session with using paypal is executed successfully");

	}
	
	@AfterClass
	public void quitBrowser() {
		Log.endLog("Test is ending");
		driver.quit();
	}
}