package com.fitbase.qa.checklistTestcases;

import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.fitbase.qa.actions.SignOutAction;
import com.fitbase.qa.actions.SignUpAction;
import com.fitbase.qa.helper.Browserhelper;
import com.fitbase.qa.testdata.LoginDataProvider;
import com.fitbase.qa.utilities.Log;

public class SignUp {

	static WebDriver driver;
	
	@BeforeClass
	public void openBrowser() throws Exception {
		
		Log.startLog("Test is starting");
		driver = Browserhelper.openBrowser();
	}
	
	@Test(dataProvider="UserSignup", dataProviderClass=LoginDataProvider.class)
	public void signUpFunctionality(String fName, String lName, String pwsd) throws Exception{
		
		SignUpAction.fitbaseSignUpflow(driver, fName, lName, pwsd);
		//SignUpAction.skipHealthDetails(driver);
		SignUpAction.fillHealthDetails(driver);
		SignUpAction.verifyDashboard(driver);
		SignOutAction.signout(driver);
		Reporter.log("Sign up testcase has executed successfully...");
		
	}
	
	@AfterClass
	public void quitBrowser() {
		
		Log.endLog("Test is ending");
		driver.quit();
	}
}
