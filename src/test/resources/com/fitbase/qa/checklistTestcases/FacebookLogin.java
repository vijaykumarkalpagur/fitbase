package com.fitbase.qa.checklistTestcases;

import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.fitbase.qa.actions.LoginAction;
import com.fitbase.qa.actions.SignOutAction;
import com.fitbase.qa.helper.Browserhelper;
import com.fitbase.qa.testdata.LoginDataProvider;
import com.fitbase.qa.utilities.Log;




public class FacebookLogin {
	
	static WebDriver driver;
	
	@BeforeClass
	public void openBrowser() throws Exception {
		
		
		Log.startLog("Test is starting");
		driver = Browserhelper.openBrowser();
	}
	
	@Test(dataProvider="FacebookLogin", dataProviderClass=LoginDataProvider.class)
	public void facebookLoginFunctionality(String facebookUsername, String facebookPassword) throws InterruptedException {
		LoginAction.fitbaseFacebookLogin(driver, facebookUsername, facebookPassword);
	//	SignOutAction.signout(driver);  
		Reporter.log("Login with Facebook testcase has been executed successfully...");

	}
	
	@AfterClass
	public void quitBrowser() {
		Log.endLog("Test is ending");
		driver.close();
	}

}
