package com.fitbase.qa.checklistTestcases;

import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.fitbase.qa.actions.LoginAction;
import com.fitbase.qa.helper.Browserhelper;
import com.fitbase.qa.testdata.LoginDataProvider;
import com.fitbase.qa.utilities.Log;


public class ForgotPassword {
	static WebDriver driver;


	@BeforeClass
	public void openbrowser() throws Exception{
				
		Log.startLog("Test is starting");
		driver = Browserhelper.openBrowser();
	}

	@Test(dataProvider="ForgotPassword", dataProviderClass = LoginDataProvider.class)
	public void forgotPasswordFunctionality(String emailAddress) throws Exception {
		
		LoginAction.fitbaseForgotPassword(driver, emailAddress);
		Reporter.log("Forgot password testcase has executed sucessfully....");

	}
	
	@AfterClass
	public void quitBrowser() {
		
		Log.endLog("Test is ending");
	    driver.quit();
	}
}
